

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include "evaluator.hh"
#include "parsefunctions.h"
#include "CMDParser.cc"
using namespace std;

stringstream usercodebuffer1; //code for before push
stringstream usercodebuffer2;  //code for after pop
stringstream breakif;

stringstream szkCodeBuffer; //what we parse in the main thing
stringstream singleNodeInSZKCodeBuffer;
stringstream singleNodeINGraphBuffer;


size_t line_count = 0;
ofstream nodeOutput;
ofstream tarjanOutput;
ofstream bfsOutput;
ofstream dfsOutput;
ofstream ramStackOutput;
ofstream queueOutput;
ofstream cIteratorOutput;

vector<string> variables;
string constructor;

bool SZK = false;
bool GRAPH= false;
//vector<string> keywords = {"$Neighbor$", "$Node$","$DfsNum$","$LowLink$" };

bool ALL = false;
constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}
Iterator* ptr;

bool iskeyword(string str){
    //cout<<"keyword: "<<str<<endl;
    for (auto a:keywords){
        //cout<<a<<endl;
        if(a==str)
            return true;
    }
    return false;
}

void parseLowerVariables( Iterator* fptr, ifstream& infile){
	cout<<"Parse Variables...\n"; 
    string word;
    string variable_name;
    string sub;
    stringstream outbuffer;
    int depth =1;
    infile>>word;
    variable_name = word;
    infile>>word;
    if(word!="in"){
        cerr<<"Syntaxerror, while declaring inner Variable: "<<variable_name<<endl;
    }
    infile>>word;
    if(word[0]!='$' || word[word.size()-1]!='$'){
        cerr<<"Expected a declared variable, like '$<variable>'$'";
        exit(1);
    }
    for(int i = 1; i < word.size()-1; i++){
        sub+= word[i]; 
        //cout<<sub<<std::endl;
    }
    Iterator* ptr_tmp = fptr;
    if(getVariable(ptr_tmp,variable_name)!=nullptr){
        cerr<<"Variable has already been declared: "<<variable_name<<endl;
        //cout<<"Variable has already been declared yet: "<<variable_name<<endl;
        exit(1);
    }    
    ptr_tmp = fptr;
    Iterator* getter = getVariable(ptr_tmp,sub);
    if(getter == nullptr){
        cerr<<"Variable has not been declared yet: "<<sub<<endl;
        //cout<<"Variable has not been declared yet: "<<sub<<endl;
        exit(1);
    }
    
    infile>>word;
    if(word !="DO"){
        cerr<<"Expected 'DO' to finish Variable declaration first: "<<variable_name<<endl;
    }
    //cout<<sub<<" : "<<getter->name<<": "<<getter->type<<endl;

            //initialize Variable
            ptr_tmp = new Iterator;
            ptr_tmp->lower = nullptr;
            ptr_tmp->bound_next = nullptr;
            ptr_tmp->name = variable_name;

    if(getter->type=="SZK"){
        SZK = true;
        while(getter->lower != nullptr){
            getter=getter->lower;
        }

        ptr_tmp->type = "SZKNODE"; //the corresponding code lands in the Poop loop
        ptr_tmp->upper = getter;
        getter->lower = ptr_tmp;
        
    }
    else if (getter->type=="GRAPH"){
        GRAPH = true;
        while(getter->lower != nullptr){
            getter=getter->lower;
        }
        ptr_tmp->type = "NODE"; //code will be executed in 
        ptr_tmp->upper = getter;
        getter->lower = ptr_tmp;

    }
    else if (getter->type =="RANGE"){
        //cout<<"found RANGE lol"<<endl;
        //cout<<"Error: cant iterate over an INTEGER"<<endl;
        cerr<<"Error: cant iterate over an INTEGER"<<endl;
        exit(1);
        
        Iterator * tmp_tmp = getter;
        while(getter->lower != nullptr){
            getter=getter->lower;
        }
        ptr_tmp->type = "RANGE_CONSTANT";
        ptr_tmp->upper = tmp_tmp;
        /*this constant should point to its range, 
        we dont do that for graphs or strong components, 
        since the variables are defined by their place at the code,
        and their scope

        */
        getter->lower = ptr_tmp;
    }
    else {
        //cout<<"wtf machst du tobi"<<endl;
        cout<<"Variable: "<<getter->name<<" hat keinen Typen"<<endl;
        exit(1);
    }
    ptr->inSCOPE=true;
    //Trennzeichen
    outbuffer<<" |»»»» "<<ptr_tmp->name<<endl; //Trennzeichen is
    while(depth&&infile>>word){
        //cout<<word<<std::endl;
        if(word=="END"){
            depth--;
            if(depth==0){

                ptr_tmp->inSCOPE=false;
                outbuffer<<" ««««| "<<ptr_tmp->name<<endl;
                break;
            }
            continue; 
            }
        if(word == "FOR"){
            //cout<<"Found For"<<endl;
            parseLowerVariables(fptr,infile);
        }
        else if(word=="PASS"){
            continue; //NO OP :) 
        }
        word = parsekeyword(word);
        //cout<<word<<std::endl;
        outbuffer<<word;
    }
    if(SZK)
        singleNodeInSZKCodeBuffer<<outbuffer.rdbuf();
    if (GRAPH){
        singleNodeINGraphBuffer<<outbuffer.rdbuf();
        //cout<<"is GRAPH";
        exit(1);
        }
    
    //cout<<"PARSELOWER END"<<endl;
}


//should be for 1for 1 parsing with variables lookup until we find another FOR 
void parseUserCode(ifstream& infile,string word){
    cout<<"Parse custom code...\n";
    int depth = 1;
    stringstream ss;
    ss<<"{";
    do
    {       
        for (int i = 0; i < word.size(); i++){
            if (word[i] == '{') depth++;
            if (word[i] == '}') depth--;
            if(depth== 0) break;
          }
        
        word = parsekeyword(word);
        //cout<<depth<<": "<<word<<endl;
        ss<<word;



    }while (depth&&infile>>word);
    if (SZK)
    {
        szkCodeBuffer<<ss.rdbuf();
    }
    if (GRAPH)
    {
        singleNodeINGraphBuffer<<ss.rdbuf();
    }
    
    if(depth>0){
        cerr<<"FILE REACHES END BEFORE   \"END\" "<<endl;
        //cout<<"FILE REACHES END BEFORE   \"END\" "<<endl;
        exit(1);
    }
    
    
}                  //out current top level variable, example x 
void parseSyntax(Iterator* ptr, ifstream& infile, string word){
	cout<<"Parsing 'For' Syntax...\n";
    int depth = 1;
    do{ 
        //cout<<"Parsefile"<<endl;
        if(word[0]=='{'){
            word = word.substr(1,string::npos);
            parseUserCode(infile,word);
        }
        if(word=="END"){
            depth--;

        }
        if(word=="PASS"){
            continue; //NO OP 
        }
        if(word=="FOR"){
            parseLowerVariables(ptr, infile);

        }
        //cout<<word<<std::endl;
    }while(depth&&infile>>word);
    if(depth>0){
        cerr<<"FILE REACHES END BEFORE   \"END\" "<<endl;
        //cout<<"FILE REACHES END BEFORE   \"END\" "<<endl;
        exit(1);
    }

    /*
    Depth Reaches ZERO PUT The Variable logic here then remove all bound variables
    */

}

void parseNode(ifstream& infile,stringstream& line_stream){
    cout<<"Parsing Node"<<endl;
    stringstream buffer;
    buffer<<"#include \"cIterator.hh\"\n";
    buffer<<"class yyNode{\n\n"<<"private: \n\n public:";
    string word;
    

    bool equals =false; 
    int depth = 1;





    if(line_stream.eof())
        infile>>word;
    else line_stream>>word;
    if(word!="{")

    std::cerr<<"Syntaxerror in Nodedefinition"<<std::endl;
    nodeOutput<<buffer.rdbuf()<<"\n";
    
    
    buffer.str(string());

    while(line_stream >> word || infile>>word){

        loop_begin:
        //cout<<word<<std::endl;
        if(word[0]=='{'){
            word= word.substr(1,word.size()-1); //ignore 1st letter
            depth+=1;
        }
        if(word[0]=='}'){
            depth-=1;
            if (depth<=0)
                {
                    break;
                    continue;
                }
            word= word.substr(1,word.size()-1); //ignore 1st letter
        }

        
        if (word[0]=='/' &&word[1]=='/'){ //see a one line comment
            if(!line_stream.eof()){
                getline(line_stream,word);
                continue;
            }
            getline(infile,word);
            continue;

        }

        if(word[0]==';'){
            nodeOutput<<buffer.rdbuf()<<";\n";
            variables.push_back(buffer.str());
            buffer.clear();
            buffer.str(string());
            word= word.substr(1,word.size()-1);
            equals = false;
            goto loop_begin;
        }

        if(word[0]!=';'&&word[0]!='='){

            if(word[word.size()-1]==';'||word[word.size()-1]=='='){
                if(word[word.size()-1]=='='){
                    equals = true;
                
                
                //case wir printen buffer
                
                    buffer<<" "<<word.substr(0,word.size()-2);
                    variables.push_back(buffer.str());
                }
                buffer<<" "<<word.substr(0,word.size()-1);
                variables.push_back(buffer.str());
                nodeOutput<<buffer.rdbuf()<<";\n";

                buffer.clear();
                buffer.str(string());
                if(equals){
                    //nodeOutput<<"=";
                    equals = false;
                }

                continue;
            } 
                //other case 
                buffer<<" " <<word;

            continue;
        }

        
        else {
            if(word[0]=='='){
                //cout<<"equals"<<endl;
                equals = true;
            }
            buffer<<" ";
            variables.push_back(buffer.str());
            //case wir printen buffer
            nodeOutput<<buffer.rdbuf();
            nodeOutput<<word;

            if(equals){
                //just print every word 1 to 1 until ';' is found
                
                buffer.clear();
                buffer.str(string());
                int a ;
                while(line_stream >> word || infile>>word){
                    if( (a = word.find(";")) != string::npos) {
                        nodeOutput<<word.substr(0,a+1) ;
                        word = word.substr(a+1,string::npos);
                        //cout<<word<<"'a':"<<a<<endl;
                        if(word.size()>0)
                            goto loop_begin;
                        break;
                    }
                    else{
                        nodeOutput<<word;
                    }
                }
                equals = false;
                }          
        }
    }

    buffer<<" ";
    nodeOutput<<buffer.rdbuf();
    buffer.clear();
    buffer.str(string());

    buffer<<"long lowLink;\n";
    buffer<<" long dfsNum;\n";


    nodeOutput<<buffer.rdbuf();
    buffer.clear();
    buffer.str(string());

    buffer<<"\n\nyyNode(";
    variables.push_back("long lowLink");
    variables.push_back("long dfsNum");
    for(size_t i=0;i<variables.size();i++){
        buffer<<variables[i]<<",";
    }
    buffer<<"yyNode* Node)";
    constructor=buffer.str();
    buffer.clear();
    buffer.str(string());



    buffer<<"; \n\nyyNode(){\n\n}\n\nyyNode( yyNode* Node, long dfsNum);\n\n~yyNode(){\n\n}";

    buffer<<"\n\n};\n\n";
    nodeOutput<<constructor<<buffer.rdbuf();
    for(string v:variables){
        //cout<<v<<"\n";
    }
    buffer.clear();
    buffer.str(string());
}

void ParsePop(ifstream& infile){
	cout<<"Parsing after Pop...\n";
    int depth = 0;
    string word;
    string tmp;
    while(
        depth>= 0 &&
        infile>>word
    ){
        loop_pop:
        std::size_t found = word.find("//");
        if (found!=std::string::npos){ //ignores one line comments
            //cout<<"one line comment"<<found<<endl;
            word= word.substr(0,found-1);
            getline(infile,tmp); //trashes rest of line
            goto loop_pop;
        }

        for(int i= 0 ; i<word.size() ; i++){
            
            if(word[i]=='{'){
                depth++;
                //cout<<"DEATH"<<endl;
                }
            if(word[i]=='}' ){
                depth--;
            }
        }
        if(depth<0) break;
                
        usercodebuffer2<<parsekeyword(word);
        //cout<<word<<usercodebuffer2.rdbuf()->in_avail()<<endl;
        
    }
}

void parsePush(ifstream& infile){
    string word;
    string tmp;
    stringstream buffer;
    stringstream singleNodebuffer;
    int depth = 0;
    singleNodebuffer<<"\nyyNode::yyNode( yyNode* Node, long dfsNum){\n\tthis->dfsNum = dfsNum;\n\t"; 
    buffer<<"yyNode::"<<constructor<<"{\n\n\t"<<"this->dfsNum = dfsNum;\n\tthis->lowLink = lowLink;\n\t";
    cout<<"Parsing after Push..."<<endl;
    while(true){
        infile>>word;
        if(word =="POP"){
            if(depth>1){
            std::cerr<<"Syntax error, missing '}' somewhere"<<std::endl;
            //std::cout<<"Syntax error, missing '}' somewhere"<<std::endl;
            }
            //std::cout<<"\n"<<word;
            //std::cout<<" "<<buffer.rdbuf()->in_avail();
            //std::cout<<"POP"<<std::endl;
            //std::cout<<"POOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOP\n"<<std::endl;
            //ParsePop(infile);
            //parsePush(infile);
            // return;
            break;            

        }
        getline(infile, tmp,';');
        
        word+=tmp;

        word.erase(std::remove_if(word.begin(), word.end(),
                            [](char c){
                                return (c == ' ' || c == '\n' || c == '\r' ||
                                        c == '\t' || c == '\v' || c == '\f');
                            }),
                            word.end());
        //cout<<word<<endl;
loop_push:
        std::size_t found = word.find("//");
        if (found!=string::npos){ //ignores one line comments
            //cout<<"one line comment xd"<<found<<endl;
            word= word.substr(0,found-1);
            getline(infile,tmp); //trashes rest of line
            goto loop_push;
        }
        if(word[word.size()-1]==';')
        word+="\n\t";
        if(word[0]=='{'){
            word= word.substr(1,word.size()-1); //ignore 1st letter
            depth+=1;
            goto loop_push;
        }
        if(word[0]=='}'){
            depth-=1;
            if (depth<=0)
                {
                    break;
                    continue;
                }
            word= word.substr(1,word.size()-1); //ignore 1st letter
            goto loop_push;
        }
        if(word[word.size()-1]==';')
        word+="\n\t";

            /* variable filtering and replacing below




            */
        {   
            loop_count:
            vector<int> indexes;
            int pos =0;
            int index = 0;
            int count = 0;
            //std::cout<<std::endl<<word<<std::endl;
            while((index = word.find("$", pos)) != string::npos) {
                count++;
                //std::cout << "Match found at position: " << index << endl;
                pos = index + 1;
                indexes.push_back(index);
            }
            if(count%2==0){
                /*
                    Todo: 
                    
                    replacing_ iterator variables
                    
                    complete constructor variables
                
                */
                for(int i = 0; i < indexes.size(); i+=2){
                    
                    string sub;
                    for(size_t u =indexes[i];u<=indexes[i+1];u++ )
                    sub+=word[u];
                    

                    //cout<<sub<<std::endl;
                    string a = word.substr(0, indexes[i]);
                    string b = word.substr(indexes[i+1]+1, string::npos);
                        if(sub=="$Node$"){
                            if(b.size()&& b[0]=='.')
                            {
                                word = a+"(*Node)"+b.substr(1,string::npos);
                            }
                            word=a+"(*Node)"+b;
                        }
                        else if(sub=="$Neighbor$"){
                            //cout<<b<<endl;
                            if(b.size()&& b[0]=='.')
                            {
                                word = a+"this->"+b.substr(1,string::npos);
                            }
                            else word= a+"(*this)"+b;
                        }
                        else if(sub=="$DfsNum$"){
                            word= a+"dfsNum"+b;
                        }
                        else if(sub=="$LowLink$"){
                            word = a+"LowLink"+b;
                        }


                    else {
                        //sub = word.substr(indexes[i],indexes[i+1]);
                        //cout<<sub<<endl;
                        sub=sub.substr(1,sub.size()-2);
                        word= a+"iterator_"+sub+b;

                    }
                     if(count>1){
                        indexes.empty();
                        //cout<<word<<endl;
                        goto loop_count;
                        }
                    if(count == 0)exit(1);
               }   
            }
            else
            {
                cerr<<"Syntaxfehler in PUSH";
            }
            
        }


        //cout<<"\n"<<word;
        word+=";\n\t";
        buffer<<word;
        singleNodebuffer<<word;
        //cout<<" "<<buffer.rdbuf()->in_avail();

    }
    buffer<<"\n\t}";
    singleNodebuffer<<"\n\t}";
    //cout<<"HOW MANY? "<<std::endl<<buffer.rdbuf()->in_avail();
    nodeOutput<<buffer.rdbuf()<<singleNodebuffer.rdbuf();
    buffer.clear();
    buffer.str(string());
    //cout<<"POOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOP\n";
    ParsePop(infile);
}


void TransitionToGreatness(ifstream& infile, stringstream& line_stream){
    string word; string tmp;
    stringstream buffer;

    bool equals =false;  
    int depth = 1;
    cout<<"Parsing Transition...\n";
    if(line_stream.eof())
        infile>>word;
    else line_stream>>word;
    if(word!="{")
        std::cerr<<"Syntaxerror on line in Nodedefinition"<<line_count<<std::endl;
    
    buffer.str(string());

    while(line_stream >> word || infile>>word){
        //cout<<word<<endl;
        loop_transition:

        std::size_t found = word.find("//");
        if(found!=std::string::npos){
            if(!line_stream.eof()){
                getline(line_stream,word);
                continue;
            }
            getline(infile,word);
            word = word.substr(0,found-1);
            goto loop_transition;
            continue;
        }
        if(word[0]=='{'){
            usercodebuffer1<<'{';
            word= word.substr(1,word.size()-1); //ignore 1st letter
            depth+=1;
            goto loop_transition;
        }
        if(word[0]=='}'){
            usercodebuffer1<<'}';
            depth-=1;
            if (depth<=0)
                {
                    break;
                    continue;
                }
            word= word.substr(1,word.size()-1); //ignore 1st letter
            goto loop_transition;
        }
        if(word == "PUSH"){
            if(depth>1){
            std::cerr<<"Syntax error, missing \'}\' somewhere"<<std::endl;
            exit(1);
            }
            //cout<<"PARSEPUSH"<<endl;
            parsePush(infile);
            break;            

        }
        else
        {
            usercodebuffer1<<parsekeyword(word);
        }
        

    }


    
} 


                                                                    //restwort
void parseBoundVariables(Iterator* ptr, stringstream& line_stream, string word ) {
    Iterator* ptr_tmp = ptr;

    int depth =0;
    while(word.size()==0) //word is given empty 
        line_stream>>word;

    if(word[0]!='('){
        
        cerr<<"Syntax error: no '(':\n";
        cerr<<word;
        exit(1);
    }
    else{
        //cout<<"No syntax error\nStarting the parse:\n";
        word=word.substr(1,string::npos);
        ++depth;
    }
    while(word.size()==0) //word is given empty 
        line_stream>>word;
    //cout<<word<<std::endl;

    do
    {   
        loop_begin:
        if(word[0]==')'){
            --depth;
            if(word.size()==1)
                continue;
            
        }
        if(word =="FOR"){
            line_stream>>word;
            //cout<<word<<std::endl;
            /*
            Insert multiple variables here until you find in
            */
           /*
           Till here
           */
    
           while(ptr_tmp->bound_next!=nullptr){
               ptr_tmp = ptr_tmp->bound_next;

           }
            Iterator* bound_next;
            bound_next = new Iterator; 
            bound_next->upper=ptr_tmp;
            bound_next->bound_next = nullptr;
            //fix this tomorrow
            bound_next->name = word;
            //strncpy(word.c_str(),bound_next->name,word.size());
            
            ptr_tmp->bound_next =bound_next;
            ptr_tmp = ptr_tmp->bound_next;
            //cout<<"pointer name: "<<ptr_tmp->name<<std::endl;
            line_stream>>word;
            //cout<<word<<std::endl;
            if (word!="in"){
                cerr<<"Expected 'in' :"<<word<<endl;

            }
            line_stream>>word;
            int find = word.find("$Range");
            if(find== 0){
                word = word.substr(find+6,string::npos);
                //cout<<word<<std::endl;
                string tmp;
                while(word.find(")$")==string::npos){
                    line_stream>>tmp;
                    word+=tmp;
                    //cout<<word.find(")$")<<std::endl;
                }
                bound_next->type ="RANGE";
                //cout<<word<<std::endl;
                if(word[word.size()-1 ]!='$' and word[word.size()-2 ]!=')'){
                    cerr<<"Probably some Syntax error m8"<<endl;
                }



                find = 0;
                int count = -1;
                while(find !=string::npos){
                    count++;
                    find = word.find(",",find+1);
                }
                //cout<<count<<std::endl;

                if(count== 0){
                    word = word.substr(1,word.find(")")-1);
                    //cout<<word<<std::endl;
                    ptr_tmp->start= 0;
                    ptr_tmp->end= stoi(word);// str to int
                }
                else if(count==1){
                    int i =word.find(",",0);
                    //cout<<word.substr(1,i-1)<<std::endl;
                    ptr_tmp->start=stoi(word.substr(1,i-1));
                    int j =word.find(",",i);
                    //cout<<word.substr(i+1,j)<<std::endl;
                    ptr_tmp->end=stoi(word.substr(i+1,j));

                    ptr_tmp->step = 0;
                    //cout<<word<<std::endl;
                    //cout<<ptr_tmp->start<<" "<<ptr_tmp->end<<endl;

                }
                else if (count==2){
                    int i =word.find(",",0);
                    //cout<<word.substr(1,i-1)<<std::endl;
                    ptr_tmp->start=stoi(word.substr(1,i-1));
                    int j =word.find(",",i+1);
                    //cout<<word.substr(i+1,j)<<std::endl;
                    ptr_tmp->end=stoi(word.substr(i+1,j));

                    ptr_tmp->step =stoi(word.substr(j+1,string::npos));
                    //cout<<word<<std::endl;
                    //cout<<ptr_tmp->start<<" "<<ptr_tmp->end<<" "<<ptr_tmp->step<<endl;


                }
                else
                {
                    cerr<<"Too many ',' in the Range operator";
                    //exit(1);
                }
                

 


            }
            /*
                Range parsing
            
            */

            line_stream>>word;
            //cout<<word<<std::endl;
           
            if (word.substr(0,2)!="DO"){
                cerr<<"Expected 'DO' :"<<word<<" : "<<line_count<<endl;

            }
            word = word.substr(2,string::npos);
            goto loop_begin;
            

            

        }
        else
        {
            cerr<<"Statement must start with 'FOR': "<<word<<endl;
        }
        //cout<<word<<std::endl;
        

    } while (line_stream>>word&&depth>0);
    //cout<<"endloop"<<depth<<std::endl;
    
    if(depth<0){
        cerr<<"Syntaxerror: ')' is an unnessary symbol "<<endl;
    }
    
    line_stream>>word;
    if(word != "DO"){
        cerr<<"Syntax error: Iterator variable declaration must end with 'DO'";
    }
    //printVariable(ptr_tmp);
    //exit(1);

}




int main(int argc, char const *argv[])
{

    CMDArgs* args = new CMDArgs;
    {
        args->directory = "./yyGraphGenerator";
        args->file = "config.txt";
        args->threads = 1;
        args->debug = false;

        parseArgs(args, argc, argv);
        

    std::cout << "Directory: " << args->directory << std::endl;
    std::cout << "File: " << args->file << std::endl;
    std::cout << "Threads: " << args->threads << std::endl;
    std::cout << "Debug: " << args->debug << std::endl;


    }

    //207
    string word; string line;
    string filename = args->file;
    cout<<"Parsing: "<<filename<<endl;
    ifstream infile;
    stringstream line_stream;
    nodeOutput.open("./yyGraphGenerator/yyNode.hh");
    nodeOutput<<"#pragma once\n";
    nodeOutput<<"#include \"yyIncludes.hh\"\n";

    ptr= new Iterator;
    ptr->name="TOP";
    ptr->type="top";
    Iterator* cpy = ptr;
    infile.open(filename);
    
    for(line;getline (infile, line);){
        line_count++;
        //cout<<line;
        line_stream= stringstream(line);


        while(line_stream>>word){
            
            if(word == "Memory"){
                line_stream>>word;
                if(word!="="){
                cerr<<"Eigentlich ein Syntaxerror *kreuzt Finger*\n";
                }
                line_stream>>word;
                if(word=="{")
                    parseMemory(infile);
                else{
                    cerr<<"SyntaxError, expected '{' in the same line"<<endl;
                }
            }
            if(word =="White"){
                line_stream>>word;
                if(word!="="){
                cerr<<"Eigentlich ein Syntaxerror *kreuzt Finger*\n";
                }
                line_stream>>word;
                if(word=="{")
                    parseWhite(infile);
                else{
                    cerr<<"SyntaxError, expected '{' in the same line"<<endl;
                }
            }
            if(word =="BreakIf"){
                line_stream>>word;
                if(word!="="){
                cerr<<"Eigentlich ein Syntaxerror *kreuzt Finger*\n";
                }
                line_stream>>word;
                if(word=="{")
                    parseBreakif(infile);
                else{
                    cerr<<"SyntaxError, expected '{' in the same line"<<endl;
                }
            }
            if(word == "Node"){
                //cout<<"Node\";
                line_stream>>word;
                if(word!="="){
                cerr<<"Eigentlich ein Syntaxerror *kreuzt Finger*\n"<<"line: "<<line_count;
                }
                parseNode(infile, line_stream);
            }
            if(word=="Transition"){
                //cout<<word<<std::endl;
                line_stream>>word;
                if (word != "=")
                {
                    cerr<<"Eigentlich ein Syntaxerror ";
                    exit(1);
                }
                
                TransitionToGreatness(infile, line_stream);
            }
            if(word == "Sources")
            {
                line_stream>>word;
                if(
                    word!="="&&
                    line_stream>>word&&
                    word!="{"
                ){
                    //cout<<"Eigentlich ein Syntaxerror"<<std::endl;

                }
                parseIncludes(infile);
            }
            if (word == "Searchinsert"){
                parseSearchInsert(line_stream);
            }
            if(word=="FOR"){

                //set syntax error for trying to make 2 files at once
                line_stream>>word;
                switch(str2int(word.c_str())){
                    case str2int("ALL"):{
                        //std::cout<<"ALL in line "<<line_count<<"."<<std::endl;
                        line_stream>>word;
                        string name = word;
                        line_stream>>word;
                        //std::cout<<word<<std::endl;
                    
                        if (word =="in"){
                            line_stream>>word;
                            int index1 =word.find("$SZK$", 0);
                            // Für Tarjan
                            if (index1>=0){
                                while(ptr->lower != nullptr){
                                    ptr = ptr->lower;
                                }
                                ptr->lower = new Iterator;
                                ptr->lower->upper = ptr;
                                ptr = ptr->lower;
                                ptr->type="SZK";
                                ptr->name = name;
                                ptr->inSCOPE = true;
                                //strncpy(ptr->name, name.c_str(),name.size());
                                SZK= true;
                                //Debugg
                                word=word.substr(5+index1,string::npos);
                                //std::cout<<"wort: "<<word<<std::endl;
                                //std::cout<<"Pointer name: "<<ptr->name<<endl;
                                parseBoundVariables(ptr,line_stream,word);
                                parseSyntax(ptr,infile,word);
                                ptr->inSCOPE = false;


                            }
                            //Für BFS/DFS
                            else if ((index1 =word.find("$GRAPH$", 0))==0){
                                while(ptr->lower != nullptr){
                                    
                                    ptr = ptr->lower;
                                }
                                ptr->lower = new Iterator;
                                ptr->lower->upper = ptr;
                                ptr = ptr->lower;
                                ptr->type="GRAPH";
                                ptr->name = name;
                                ptr->inSCOPE = true;
                                GRAPH = true;
                                word=word.substr(7+index1,string::npos);
                                parseBoundVariables(ptr,line_stream,word);
                                parseSyntax(ptr,infile,word);

                                ptr->inSCOPE = false;
                                
                            }
                            //Inner Variables 
                            else if ((index1 =word.find("$Range", 0))==0){
                                while(ptr->lower != nullptr){
                                    ptr = ptr->lower;
                                }
                                ptr->lower = new Iterator;
                                ptr->lower->upper = ptr;
                                ptr = ptr->lower;
                                ptr->type="RANGE";
                                ptr->name = name;
                                ptr->inSCOPE = true;
                                word=word.substr(6+index1,string::npos);
                                parseBoundVariables(ptr,line_stream,word);
                                parseSyntax(ptr,infile,word);
                                ptr->inSCOPE = false;
                            }
                            else{
                                cerr<<"Unknown: Variable Type: "<<word<<endl;
                                cerr<<"Possible are $RANGE(...)$,$GRAPH$, and $SZK$"<<endl;
                                exit(1);
                            }
                            /*cout<<"Mein Index: "<< index1<<endl;
                            int index2 = word.find("$Range(", 0);
                            cout<<"Mein Index2: "<< index2<<endl;
                            */
                        }
                        else{
                            cerr<<"Syntaxerror: Expected 'in', not "<<word<<endl;
                            exit(1);
                        }
                        ALL = true;
                        break;
                    }
                    default :{
                        cerr<<"Syntaxfehler in line "<<line_count<<": "<<word<<" kein Schlüsselwort"<<endl;
                        break;
                        }
                }
                
            }
        }
        

        
    }

    //cout<<"close"<<endl;
    //cout<<"buffer1"<<endl;
    //if(usercodebuffer1.str().size())
        //cout<<usercodebuffer1.rdbuf()<<std::endl;
    //cout<<"buffer2"<<endl;
    //if(usercodebuffer2.str().size())
    //cout<<usercodebuffer2.rdbuf()<<endl;
    //cout<<whiteBuffer.rdbuf()<<std::endl;
    //cout<<memoryBuffer.rdbuf()<<std::endl;
    if(!SZK)
        cout<<"No SZK"<<endl;
    if(SZK){

        cout<<"Creating a Tarjan Algorithm..."<<endl;
        tarjanOutput.open("./yyGraphGenerator/yyTarjan.hh");
        linker(
            ptr,
            usercodebuffer1,
            usercodebuffer2,
            szkCodeBuffer,
            singleNodeInSZKCodeBuffer,
            singleNodeINGraphBuffer,
            breakif,
            memoryBuffer,
            whiteBuffer,
            nodeOutput,
            tarjanOutput,
            ramStackOutput,
            cIteratorOutput,
            Includ,
            searchinsertbuffer,
            BreakifBuffer
        );
        }
    else if(GRAPH){
dfsOutput.open("./yyGraphGenerator/yyDFS.hh");
bfsOutput.open("./yyGraphGenerator/yyBFS.hh");
        cout<<"Creating a Depth-First-Search and Breath-First-Search Algorithm..."<<endl;
        linker_dfs_bfs( ptr,  //variable tree
            usercodebuffer1, //code for before push
            usercodebuffer2,  //code for after pop
            singleNodeINGraphBuffer,
            breakif,
            memoryBuffer,
            whiteBuffer,
            nodeOutput,
            dfsOutput,
            bfsOutput,
            ramStackOutput,
            queueOutput,
            cIteratorOutput,
            Includ,
            searchinsertbuffer,
            BreakifBuffer
    );
    }

    nodeOutput.close();
    //tarjanOutput<< szkCodeBuffer.rdbuf()<<std::endl;
    //tarjanOutput<< singleNodeInSZKCodeBuffer.rdbuf()<<std::endl;
    
    tarjanOutput.close();
    dfsOutput.close();
    bfsOutput.close();
    cout<<"ENDE"<<endl;


    delete args;
    delete ptr;


    return 0;
}
