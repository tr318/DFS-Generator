#pragma once
#include <iostream>
#include <string>

struct CMDArgs {
    std::string directory;
    std::string file;

    int threads;
    bool debug;
};

void parseArgs(CMDArgs* args, int argc, char const *argv[]) {
    int index = 1;
    while(index < argc) {
        std::string key = std::string(argv[index]);

        if(key == "-d" || key == "--directory") {
            args->directory = argv[index + 1];
            index += 2;
        }
        else if(key == "-t" || key == "--threads") {
            args->threads = atoi(argv[index + 1]);
            if(args->threads <= 0) {
                std::cerr << "Value for option --threads is not valid." << std::endl;
                exit(1);
            }

            index += 2;
        }
        else if(key == "--debug") {
            args->debug = true;
            index += 1;
        }
        else if(key == "--help") {
            std::cout << "Valid commandline arguments:" << std::endl;
            std::cout << "-d (--directory):\t Specify folder" << std::endl;
            std::cout << "-t (--threads):\t\t Specify amount of threads used for computing" << std::endl;
            std::cout << "--debug:\t\t Activate debug mode" << std::endl;
            std::cout << "--help:\t\t\t Display this help text" << std::endl;
            exit(0);
        }
        else {
            // If the string doesn't start with a dash, we assume, that this is the file we want to process
            if(key[0] != '-') {
                args->file = argv[index];

                // Ignore all the following arguments
                break;
            }
            
            // Otherwise the given argument is not supported by the parser
            else {
                std::cerr << "Couldn't recognize argument: " << key << std::endl;
                exit(1);
            }
        }
    }
}
