#include <vector>


enum tarjanCriticalInserts {
    Breakif = 3,
    FirstNeighbor=4,
    Searchinsert =5,
    NextNeighbor =6,
    SZKCode = 7,
    SZKNodeCode = 8
    };
const char* codeTemplateTarjan[] = {
    "#pragma once\n\
\n\
#include <iostream>\n\
#include \"ramStack.hh\"\n\
#include \"yyNode.hh\"\n\
#include <list>\n\
#include <vector>\n\
\n\
\n\
size_t LongsPerBlock;\n\
size_t BoolsPerBlock;\n\
struct onStackLookup {\n\
\tint index_start, index_end;\n\
\tbool* table;\n\
\tonStackLookup* next = nullptr;\n\
\t~onStackLookup(){\n\
\t\tstd::cout << \"Destructing OnStackLookUp...\\n\";\n\
\t\tstd::free(table);\n\
\t}\
};\n\
\n\
onStackLookup osLookup;\n\
\n\
bool isOnStack(int dfsNum) {\n\
\tonStackLookup* ptr = &osLookup;\n\
\twhile (dfsNum > ptr->index_end)\n\
\t{\n\
\t\tptr = ptr->next;\n\
\t}\n\
\tif (ptr->index_start == 0) {\n\
\t\treturn ptr->table[dfsNum] == 1;\n\
\t}\n\
\treturn ptr->table[dfsNum % ptr->index_start] == 1;\n\
}\n\
\n\
void addDfsNum(int dfsNum, bool val) {\n\
\tonStackLookup* ptr = &osLookup;\n\
\tint seen = 0;\n\
\twhile (dfsNum > ptr->index_end)\n\
\t{\n\
\t\tif (ptr == nullptr || ptr->index_start < 0 || ptr->index_end < 0) {\n\
\t\t\tstd::cerr << \"pointer is null\" << std::endl;\n\
\t\t\texit(1);\n\
\t\t}\n\
\t\tif (ptr->next == nullptr) {\n\
\t\t\tptr->next = (onStackLookup*)std::malloc(sizeof(onStackLookup));\n\
\t\t\tonStackLookup lookUp;\n\
\t\t\tptr->next->index_start = ptr->index_end + 1;\n\
\t\t\tptr->next->index_end = ptr->index_end + BoolsPerBlock;\n\
\t\t\t//ptr->next->table = new bool[BoolsPerBlock];\n\
\t\t\tptr->next->table = (bool*)std::calloc(BoolsPerBlock, sizeof(bool));\n\
\t\t\tptr->next->next = nullptr;\n\
\t\t\tif (lookUp.table == nullptr || lookUp.table == NULL) {\n\
\t\t\t\tstd::cerr << \"could not allocate memory\" << std::endl;\n\
\t\t\t\texit(1);\n\
\t\t\t}\n\
\t\t}\n\
\t\tptr = ptr->next;\n\
\t\tseen++;\n\
\t}\n\
\tif (ptr->index_start == 0) {\n\
\t\tptr->table[dfsNum] = val;\n\
\t\t//std::cout << dfsNum << std::endl;\n\
\t\treturn;\n\
\t}\n\
\t//std::cout << dfsNum % ptr->index_start << std::endl;\n\
\tptr->table[dfsNum % ptr->index_start] = val;\n\
}\n\
\n\
struct lowlinkLookup {\n\
\tint index_start, index_end;\n\
\tlong* table;\n\
\tlowlinkLookup* next = nullptr;\n\
~lowlinkLookup(){\n\
\t\tstd::cout <<\"Destructing LowLinkLookup...\\n\";\n\
\t\tstd::free(this->table);\n\
\t}\n\
};\n\
\n\
lowlinkLookup llLookup;\n\
\n\
long getLowLink(int dfsNum) {\n\
\tlowlinkLookup* ptr = &llLookup;\n\
\twhile (dfsNum > ptr->index_end)\n\
\t{\n\
\t\tptr = ptr->next;\n\
\t}\n\
\tif (ptr->index_start == 0) {\n\
\t\treturn ptr->table[dfsNum];\n\
\t}\n\
\treturn ptr->table[dfsNum % ptr->index_start];\n\
}\n\
\n\
void setLowLink(int dfsNum, long val) {\n\
\tlowlinkLookup* ptr = &llLookup;\n\
\tint i = 0;\n\
\twhile (dfsNum > ptr->index_end)\n\
\t{\n\
\t\tif (ptr == nullptr || ptr->index_start < 0 || ptr->index_end < 0) {\n\
\t\t\tstd::cerr << \"pointer is null\" << std::endl;\n\
\t\t\texit(1);\n\
\t\t}\n\
\t\tif (ptr->next == nullptr) {\n\
\t\t\tptr->next = (lowlinkLookup*)std::malloc(sizeof(onStackLookup));\n\
\t\t\tlowlinkLookup lookUp;\n\
\t\t\tptr->next->index_start = ptr->index_end + 1;\n\
\t\t\tptr->next->index_end = ptr->index_end + LongsPerBlock;\n\
\t\t\t//ptr->next->table = new bool[LongsPerBlock];\n\
\t\t\tptr->next->table = (long*)std::calloc(LongsPerBlock, sizeof(long));\n\
\t\t\tptr->next->next = nullptr;\n\
\t\t\tif (lookUp.table == nullptr || lookUp.table == NULL) {\n\
\t\t\t\tstd::cerr << \"could not allocate memory\" << std::endl;\n\
\t\t\t\texit(1);\n\
\t\t\t}\n\
\t\t}\n\
\t\tptr = ptr->next;\n\
\t\ti++;\n\
\t}\n\
\tif (ptr->index_start == 0) {\n\
\t\tptr->table[dfsNum] = val;\n\
\t\t//std::cout << dfsNum << std::endl;\n\
\t\treturn;\n\
\t}\n\
\t//std::cout << dfsNum % ptr->index_start << std::endl;\n\
\tptr->table[dfsNum % ptr->index_start] = val;\n\
}\n\
void freeOsLookup() {\n\
    onStackLookup* osTmp = &osLookup;\n\
    onStackLookup* osTmp2;\n\
    std::free(osTmp->table);\n\
    osTmp = osTmp->next;\n\
    while (osTmp != nullptr) {\n\
        osTmp2 = osTmp;\n\
        osTmp = osTmp2->next;\n\
        std::free(osTmp2->table);\n\
    }\n\
}\n\
void freellLookup() {\n\
    lowlinkLookup* osTmp = &llLookup;\n\
    lowlinkLookup* osTmp2;\n\
    std::free(osTmp->table);\n\
    osTmp = osTmp->next;\n\
    while (osTmp != nullptr) {\n\
        osTmp2 = osTmp;\n\
        osTmp = osTmp2->next;\n\
        std::free(osTmp2->table);\n\
    }\n\
}\n\
void test() {\n\
\tfor (int i = 0; i < 20000; i++) {\n\
\t\taddDfsNum(i, true);\n\
\t}\n\
\tfor (int i = 0; i < 20000; i++) {\n\
\t\tstd::cout << isOnStack(i) << std::endl;\n\
\t}\n\
}\n\
\n\
\n\
\n\
struct viTuple {\n\
\ttuple t;\n\
};\n\
\n\
void yyTarjan(yyNode* firstNode) {\n",
"\
\tllLookup.index_start = 0;\n\
\tllLookup.index_end = LongsPerBlock - 1;\n\
\tllLookup.next = nullptr;\n\
\tllLookup.table = (long*)std::malloc(LongsPerBlock * sizeof(long));\n\
\tosLookup.index_start = 0;\n\
\tosLookup.index_end = BoolsPerBlock - 1;\n\
\tosLookup.next = nullptr;\n\
\tosLookup.table = (bool*)std::malloc(BoolsPerBlock * sizeof(bool));\n\
\tint next = 0; // next index\n\
\tramStack<tuple> stack(ItemsPerBlock);\n\
\tramStack<viTuple> work(ItemsPerBlock); //Recursion stack\n\
\ttuple v;\n\
\ttuple w;\n\
\tint seen;\n\
\tint j;\n\
\tyyNode nextNode;\n\
\tbool recurse=true;\n\
\tviTuple vi;\n\
\tvi.t.n = *firstNode;\n\
\t//vi.seen = 0;\n\
", //vi.v = ?? -> firstNode, zweifelsfall = 0 

        "\twork.push(&vi);\n\
\twhile (!work.empty()) {\n\
\t\tv = work.Top()->t;\n\
\t\tv.n.lowLink = getLowLink(v.n.dfsNum);\
\t\t//seen = work.Top()->seen; // i is next successor to process\n\
\t\twork.pop();\n",
"\
\t\tif (recurse) { //When first visiting a Vertex\n\
\t\t\tv.n.dfsNum = next;\n\
\t\t\tv.n.lowLink = next;\n\
\t\t\tsetLowLink(next,next);\n\
\t\t\tstack.push(&v);\n\
\t\t\t//v.n->onStack = true;\n\
\t\t\taddDfsNum(next, true);\n\
\t\t\tnext++;\n\
\t\t}\n\
\t\trecurse = false;\n\
\n\
\t\tyyNode nextNode = v.n;\n\
\t\twhile (1",") {\n", //breakif function
            
"\t\t\t//successor method\n\
\t\t\t//w.n = graph->head[nextPtr->val];\n\
\t\t\ttuple tmp_tpl;\n\
\t\t\tw = tmp_tpl;\n\
\t\t\tw.n = nextNode;\n",
//search insert
"\t\t\t\tw.n.lowLink=getLowLink(w.n.dfsNum);\n\
\t\t\tif (true&&w.n.dfsNum==-1\n",
            
            
            ") { // search-insert here\n",//usercode 1 here
            "\
\t\t\t\t//Add w to recursion stack.\n\
\t\t\t\tviTuple v_tmp;\n\
\t\t\t\tv_tmp.t = v;\n\
\t\t\t\t//v_tmp.seen = 1;\n\
\t\t\t\twork.push(&v_tmp);\n\
\t\t\t\tviTuple v_tmp2;\n\
\t\t\t\tv_tmp2.t = w;\n\
\t\t\t\t//v_tmp2.seen = 0;\n\
\t\t\t\twork.push(&v_tmp2);\n\
\t\t\t\trecurse = true;\n\
\t\t\t\tbreak;\n\
\t\t\t}\n\
\t\t\telse if (isOnStack(w.n.dfsNum)) {\n\
\t\t\t\tsetLowLink(v.n.dfsNum , getLowLink(v.n.dfsNum) < w.n.dfsNum ? getLowLink(v.n.dfsNum) : w.n.dfsNum);\n\
\t\t\t}\n\
\t\t\t//nextPtr = nextPtr->next;\n\
", // next Neighbor, 2nd user code after while loop
"\t\t}\n",
"\t\tif (recurse) continue;\n\
\t\tif (v.n.dfsNum == getLowLink(v.n.dfsNum)) {\n\
\t\t\t// pop loop\n\
",
                //user-generated code on all Nodes

                "\t\t\t//std::list<adjNode*> scc;\n\
\t\t\twhile (true) {\n\
\t\t\t\tw = *stack.Top();\n\
\t\t\t\tw.n.lowLink=getLowLink(w.n.dfsNum);\n\
\n\
\n\
\t\t\t\t//w->onStack = false;\n\
\t\t\t\taddDfsNum(w.n.dfsNum, false);\n\
",
                        //user generated code on SZK-Nodes
"\t\t\t\t//scc.push_back(w.n);\n\
\n\
\t\t\t\tif (w.n.dfsNum == v.n.dfsNum) {\n\
\t\t\t\t\tstack.pop(); //because v must be poped aswell, \n\
\t\t\t\t\tbreak;\n\
\t\t\t\t}\n",
"\t\t\t\tstack.pop(); // remove w's reference from stack \n\
\t\t\t}\n\
\t\t\t//printScc(scc);\n\
\t\t}\n\
",
            "\t\tif (!work.empty()) {\n\
\t\t\tw = v;\n\
\t\t\tv = work.Top()->t;\n\
\t\t\tsetLowLink(v.n.dfsNum, getLowLink(v.n.dfsNum) < getLowLink(w.n.dfsNum) ?  getLowLink(v.n.dfsNum) :  getLowLink(w.n.dfsNum));\n\
\t\t\t//v.n.lowLink = v.n.lowLink < w.n.lowLink ? v.n.lowLink : w.n.lowLink;\n\
\t\t\t//work.Top()->t.n.lowLink = v.n.lowLink < w.n.lowLink ? v.n.lowLink : w.n.lowLink;\n\
\t\t}\n\
\t}\n\
//freeOsLookup();\n\
//freellLookup();\n\
}\n\
"
};

enum DFSCriticalInserts {UserCode};

const char* codeTemplateDFS[]= {
"#pragma once\n\
\n\
#include \"ramStack.hh\"\n\
#include \"yyNode.hh\"\n\
#include <iostream>\n\
\n\n\
size_t LongsPerBlock;\n\
size_t BoolsPerBlock;\n\
struct onStackLookup {\n\
\tint index_start, index_end;\n\
\tbool* table;\n\
\tonStackLookup* next = nullptr;\n\
\t~onStackLookup(){\n\
\t\tstd::cout << \"Destructing OnStackLookUp...\\n\";\n\
\t\tstd::free(table);\n\
\t}\
};\n\
\n\
onStackLookup osLookup;\n\
\n\
bool isOnStack(int dfsNum) {\n\
\tonStackLookup* ptr = &osLookup;\n\
\twhile (dfsNum > ptr->index_end)\n\
\t{\n\
\t\tptr = ptr->next;\n\
\t}\n\
\tif (ptr->index_start == 0) {\n\
\t\treturn ptr->table[dfsNum] == 1;\n\
\t}\n\
\treturn ptr->table[dfsNum % ptr->index_start] == 1;\n\
}\n\
\n\
void addDfsNum(int dfsNum, bool val) {\n\
\tonStackLookup* ptr = &osLookup;\n\
\tint seen = 0;\n\
\twhile (dfsNum > ptr->index_end)\n\
\t{\n\
\t\tif (ptr == nullptr || ptr->index_start < 0 || ptr->index_end < 0) {\n\
\t\t\tstd::cerr << \"pointer is null\" << std::endl;\n\
\t\t\texit(1);\n\
\t\t}\n\
\t\tif (ptr->next == nullptr) {\n\
\t\t\tptr->next = (onStackLookup*)std::malloc(sizeof(onStackLookup));\n\
\t\t\tonStackLookup lookUp;\n\
\t\t\tptr->next->index_start = ptr->index_end + 1;\n\
\t\t\tptr->next->index_end = ptr->index_end + BoolsPerBlock;\n\
\t\t\t//ptr->next->table = new bool[BoolsPerBlock];\n\
\t\t\tptr->next->table = (bool*)std::calloc(BoolsPerBlock, sizeof(bool));\n\
\t\t\tptr->next->next = nullptr;\n\
\t\t\tif (lookUp.table == nullptr || lookUp.table == NULL) {\n\
\t\t\t\tstd::cerr << \"could not allocate memory\" << std::endl;\n\
\t\t\t\texit(1);\n\
\t\t\t}\n\
\t\t}\n\
\t\tptr = ptr->next;\n\
\t\tseen++;\n\
\t}\n\
\tif (ptr->index_start == 0) {\n\
\t\tptr->table[dfsNum] = val;\n\
\t\t//std::cout << dfsNum << std::endl;\n\
\t\treturn;\n\
\t}\n\
\t//std::cout << dfsNum % ptr->index_start << std::endl;\n\
\tptr->table[dfsNum % ptr->index_start] = val;\n\
}\n\
\n\
void freeOsLookup() {\n\
    onStackLookup* osTmp = &osLookup;\n\
    onStackLookup* osTmp2;\n\
    std::free(osTmp->table);\n\
    osTmp = osTmp->next;\n\
    while (osTmp != nullptr) {\n\
        osTmp2 = osTmp;\n\
        osTmp = osTmp2->next;\n\
        std::free(osTmp2->table);\n\
    }\n\
}\n\
",
"void yyDFS(yyNode* firstNode) {\n",
    "\
    std::cout<<\"Starting DFS with BlockSize \"<<ItemsPerBlock<<std::endl;\n\
    osLookup.index_start = 0;\n\
	osLookup.index_end = BoolsPerBlock - 1;\n\
	osLookup.next = nullptr;\n\
	osLookup.table = (bool*)std::malloc(BoolsPerBlock * sizeof(bool));\n\
    // Initialize an empty dfs stack and push the \"start\" node into it\n\
    ramStack<tuple> stack(ItemsPerBlock);\n\
    tuple v;\n\
    v.n = *firstNode;\n\
    v.n.dfsNum=0;\n\
    size_t next= 0;\n\
    ", //init des ersten tuples
    "stack.push(&v);\n\
    yyNode elem, nextNode;\n",
    "while (!stack.empty()) {\n\
        v= *stack.Top();\n\
        stack.pop();\n",
    "if (isOnStack(v.n.dfsNum)\n", //onStack hier
    ") {\n\
            continue;\n\
        }\n\
        //elem->mark = 1;\n", //hier nextNode ==
        "\
        while (\n" , //breakif + iteration hier
        "\n) {\n", // search insert vor dem if
        "tuple w;\n\
        w.n  = yyNode(&v.n, (long) -1 );\n\
        if (w.n.dfsNum==-1\n",//white
        "){\
                //v_tmp.it = nullptr;\n",
        //user code 1 for before bfs
        "\n\
        stack.push(&v);\n\
        next++;\n\
        w.n.dfsNum=next;\n\
        stack.push(&w);\n\
        addDfsNum(v.n.dfsNum, true);\n\
        }\n\
        }\n\
        ",
        //usercode2 for after bfs
        "addDfsNum(v.n.dfsNum, false);\n\
        }\n\
        //freeOsLookup();\n\
}"
        

};
enum BFSCriticalInserts {};

const char* codeTemplateBFS[]= {
"#pragma once\n#include \"ramQueue.hh\"\n\
#include \"yyNode.hh\"\n\
#include <iostream>\n\
size_t LongsPerBlock;\n\
size_t BoolsPerBlock;\n\
struct onStackLookup {\n\
\tint index_start, index_end;\n\
\tbool* table;\n\
\tonStackLookup* next = nullptr;\n\
\t~onStackLookup(){\n\
\t\tstd::cout << \"Destructing OnStackLookUp...\\n\";\n\
\t\tstd::free(table);\n\
\t}\
};\n\
\n\
onStackLookup osLookup;\n\
\n\
bool isOnStack(int dfsNum) {\n\
\tonStackLookup* ptr = &osLookup;\n\
\twhile (dfsNum > ptr->index_end)\n\
\t{\n\
\t\tptr = ptr->next;\n\
\t}\n\
\tif (ptr->index_start == 0) {\n\
\t\treturn ptr->table[dfsNum] == 1;\n\
\t}\n\
\treturn ptr->table[dfsNum % ptr->index_start] == 1;\n\
}\n\
\n\
void addDfsNum(int dfsNum, bool val) {\n\
\tonStackLookup* ptr = &osLookup;\n\
\tint seen = 0;\n\
\twhile (dfsNum > ptr->index_end)\n\
\t{\n\
\t\tif (ptr == nullptr || ptr->index_start < 0 || ptr->index_end < 0) {\n\
\t\t\tstd::cerr << \"pointer is null\" << std::endl;\n\
\t\t\texit(1);\n\
\t\t}\n\
\t\tif (ptr->next == nullptr) {\n\
\t\t\tptr->next = (onStackLookup*)std::malloc(sizeof(onStackLookup));\n\
\t\t\tonStackLookup lookUp;\n\
\t\t\tptr->next->index_start = ptr->index_end + 1;\n\
\t\t\tptr->next->index_end = ptr->index_end + BoolsPerBlock;\n\
\t\t\t//ptr->next->table = new bool[MAXINT];\n\
\t\t\tptr->next->table = (bool*)std::calloc(BoolsPerBlock, sizeof(bool));\n\
\t\t\tptr->next->next = nullptr;\n\
\t\t\tif (lookUp.table == nullptr || lookUp.table == NULL) {\n\
\t\t\t\tstd::cerr << \"could not allocate memory\" << std::endl;\n\
\t\t\t\texit(1);\n\
\t\t\t}\n\
\t\t}\n\
\t\tptr = ptr->next;\n\
\t\tseen++;\n\
\t}\n\
\tif (ptr->index_start == 0) {\n\
\t\tptr->table[dfsNum] = val;\n\
\t\t//std::cout << dfsNum << std::endl;\n\
\t\treturn;\n\
\t}\n\
\t//std::cout << dfsNum % ptr->index_start << std::endl;\n\
\tptr->table[dfsNum % ptr->index_start] = val;\n\
}\n\
\n\
void freeOsLookup() {\n",
    "onStackLookup* osTmp = &osLookup;\n\
    onStackLookup* osTmp2;\n\
    std::free(osTmp->table);\n\
    osTmp = osTmp->next;\n\
    while (osTmp != nullptr) {\n\
        osTmp2 = osTmp;\n\
        osTmp = osTmp2->next;\n\
        std::free(osTmp2->table);\n\
    }\n\
}\n\
    void yyBFS(yyNode* firstNode) {\n",
    "osLookup.index_start = 0;\n\
	osLookup.index_end = BoolsPerBlock - 1;\n\
	osLookup.next = nullptr;\n\
	osLookup.table = (bool*)std::malloc(BoolsPerBlock * sizeof(bool));\n\
    size_t next = 0;\n\
    ramQueue<tuple> queue(ItemsPerBlock);\n\
    tuple v;\n\
    v.n = *firstNode;//get first Node\n\
    v.n.dfsNum = 0;//get first Node\n\
    std::cout<<\"Starting BFS with BlockSize \"<<ItemsPerBlock<<std::endl;\n\
    ",
    " queue.enqueue(&v);\n",
    "    yyNode nextNode;\n\
    ",
    "while (!queue.empty()) {\n\
\n\
        v = *queue.Bot();\n\
        addDfsNum(v.n.dfsNum,false);\n\
        bool has_child = true;\n", //hier initialisieren
"while (\n", //breakif + iteration
") { // while node got children\n\
tuple w;\n\
", //search-insert
    "if (w.n.dfsNum==-1",") { //we push if its not known\n\
                    //v_tmp.it =nullptr;\n\
                    next++;\n\
                    w.n.dfsNum=next;\n\
                    ",
                    //user code
                    "\n\
                    queue.enqueue(&w);\n\
                    addDfsNum(w.n.dfsNum,true);//its in queue,marked grey\n\
                    //nextNode->mark = 1;\n\
    }\n\
    ",
    "}\n\
    ",//after bfs user code
    "        queue.dequeue();\n\
    }\n\
    //freeOsLookup();\n\
}\n"





};
const char* codeTemplateRamStack[]= {"#include <cstddef>\n\
#include <stdio.h>\n\
#include <stdlib.h>\n\
#include <iostream>\n\
#include <mutex>\n\
#pragma once\n\
#define MAXINT 10000\n\
\n\
\n\
\n\
template <typename TypeT>\n\
void stackDelete(TypeT array)\n\
{\n\
\n\
    delete[] array;\n\
    //std::cout << \"stack deleted\" << std::endl;\n\
}\n\
\n\
\n\
\n\
template <typename TypeT>\n\
class ramStack\n\
{\n\
public:\n\
    TypeT** blockList = nullptr;\n\
    TypeT* bottom = nullptr;   //pointer to stack_bottom\n\
    TypeT* top = nullptr;     //pointer to the top element (not array)\n\
    std::size_t currentblock = 0; //shows which block of stack memory we are in\n\
    std::size_t height = 0; //independently of of block size shows how many elements (Type*) are in stack\n\
    std::size_t block;\n\
private:\n\
    std::size_t blocksize = 8000; //how many elements fit in one block\n\
    std::size_t blockLength; //amount of TypeT pointers per block\n\
    std::mutex mtx;\
    void appendBlock() {\n\
        ///TO DO somehow implement a next pointer in the stack class\n\
        //std::cout<<\"mounting at this->blocklist[\"<<this->currentblock+1<<\"]\"<<std::endl;\n\
\n\
        //vorher\n\
        //this->blockList[currentblock++]=stackCreate(this->blocksize);\n\
\n\
\n\
\n\
        //nachher\n\
        this->currentblock++;\n\
        this->blockList[currentblock] = stackCreate(this->blocksize);\n\
    }\n\
    void removeBlock() {\n\
        delete[] this->blockList[currentblock];\n\
        this->blockList[currentblock] = nullptr;\n\
        this->currentblock--;\n\
\n\
    }\n\
\n\
    TypeT* stackCreate(std::size_t size)\n\
    {\n\
        //std::cout<<\"Allocation this much memory: \"<<size<<std::endl;\n\
        TypeT* array = new TypeT [size];\n\
        /*for(size_t i = 0;i<size;i+=sizeof(TypeT*))\n\
            std::cout<<&array[i]<<std::endl;*/\n\
        return array;\n\
\n\
    }\n\
public:\n\
    ramStack(std::size_t n) {\n\
        this->height = 0;\n\
        this->blockList = static_cast<TypeT**>(std::malloc(sizeof(TypeT*) * MAXINT));\n\
        this->blockList[0] = stackCreate(blocksize);\n\
        this->blockLength = n;\n\
        this->blocksize = sizeof(TypeT) * n;\n\
\n\
        this->top = &this->blockList[0][0];\n\
        this->blockLength = this->blocksize / sizeof(TypeT);\n\
\n\
    }\n\
    ~ramStack() {\n\
        //std::cout << \"delete ramStack\";\n\
\n\
        //delete[] this->blockList;\n\
        while (this->currentblock)\n\
          {\n\
              std::cout<<this->currentblock;\n\
              delete[] this->blockList[currentblock];\n\
              std::cout<<\"success\\n\";\n\
              currentblock--;\n\
          }\n\
          delete[] this->blockList[0];\n\
\n\
          //std::cout<<\"Error\";\n\
          std::free( this->blockList);\n\
    \n\
    }\n\
    void push(TypeT* ptr) {\n\
\n\
mtx.lock();\n\
        //std::cout<<\"height, currentblock, sizeofblock \"<<this->height<<\", \"<<this->currentblock<<std::endl;\n\
        if (this->height > 0 && this->height % this->blockLength == 0) {\n\
            this->appendBlock();\n\
        }\n\
\n\
        //std::cout<<\"Allocating at: \"<<this->currentblock<<\" \"<<this->height%this->blockLength<<ptr<<\"\\n\";\n\
\n\
        this->blockList[currentblock][this->height % this->blockLength] = *ptr;\n\
        this->top = &this->blockList[currentblock][this->height % this->blockLength];\n\
        this->height++;\n\
        //printf(\"still running\\n\");\n\
        //std::cout<<sizeof((this->blockList[currentblock]+this->height))<<\" \"<<(this->blockList[currentblock]+this->height)<<std::endl;\n\
mtx.unlock();\n\
    }\n\
    void pop() {\n\
\n\
\n\
mtx.lock();\n\
        //this->blockList[currentblock][this->height%this->blockLength]=nullptr; // compile error, type covnersion int -> int*\n\
        if (this->height == 0)\n\
            return;\n\
\n\
        if ((this->height - 1) % this->blockLength == 0 && currentblock != 0) { // height - 1 instead of height and added currentblock != 0\n\
            this->removeBlock();\n\
            this->height--;\n\
            this->top = &this->blockList[currentblock][this->blockLength - 1]; //avoiding this one modulo operation // changed blocksize to blockLength\n\
            return;\n\
        }\n\
        this->height--;\n\
        this->top = &blockList[currentblock][(this->height - 1) % this->blockLength]; // height - 1 instead of height\n\
mtx.unlock();\
    }\n\
    TypeT* Top() {\n\
        if (!this->height) {\n\
            return nullptr;\n\
        }\n\
        return this->top;\n\
    }\n\
    TypeT* Bot() {\n\
        if (this->empty())\n\
            return nullptr;\n\
        return this->bottom;\n\
    }\n\
    bool empty() {\n\
        return this->height == 0;\n\
    }\n\
};\n\
"};

const char* codeTemplateCIterator[] = {"#include <string>\n\
\n\
 class cIterator\n\
{\n\
public:\n\
\n\
    //std::string id;\n\
    long val;\n\
    long end;\n\
    long step;\n\
    long start;\n\
    cIterator* lower=nullptr;\n\
\n\
    int next()  {\n\
        if(lower && lower->next()==true)\n\
            return true;\n\
        if (this->val==end)\n{\
            this->val = this->start;\
            return false;\n\
        }\
        this->val += 1 ;\n\
        return true;\n\
    }\n\
    size_t getValue(){\n\
        return this->val; \n\
    }\n\
    cIterator(){\n\
\n\
    }\n\
    cIterator(long end){\n\
        //this->id = id;     \n\
        this->end = end;\n\
        this->val = 0;\n\
        this->step = 1;\n\
        this->start=this->val;\n\
        //this->getValue = getValue;\n\
\n\
    }\n\
    cIterator(  long val, long end){\n\
        //this->id = id;     \n\
        this->end = end;\n\
        this->val = val;\n\
        this->step = 1;\n\
        this->start=this->val;\n\
        //this->getValue = getValue;\n\
\n\
    }\n\
    cIterator(  long val, long end, long step){\n\
        //this->id = id;     \n\
        this->end = end;\n\
        this->val = val;\n\
        this->step= step;\n\
        this->start=this->val;\n\
        //this->getValue = getValue;\n\
\n\
    }\n\
\n\
    ~cIterator(){\n\
\n\
    }\n\
\n\
};\n\
"};
const char* codeTemplateMain[]={
"int main(int argc, char const *argv[])\n\
{\n\
    ","return 0;\n\
}\n\
"
} ;

const char* codeTemplateQueue[]={
"#define MAXINT 10000\n\
#include <cstddef>\n\
#include <stdio.h>\n\
#include <stdlib.h>\n\
#include <iostream>\n\
#include <mutex>\n\
#pragma once\n\
\n\
template <typename TypeT>\n\
class ramQueue{\n\
    TypeT** blockList = nullptr;\n\
    TypeT* bottom = nullptr;   //pointer to stack_bottom\n\
    TypeT* top = nullptr;     //pointer to the top element (not array)\n\
    std::size_t blocksize=0;\n\
    std::size_t currentblock = 0; //shows which block of stack memory we are in\n\
    std::size_t topblock = 0;\n\
    std::size_t height = 0; //independently of of block size shows how many elements (Type*) are in stack\n\
    std::size_t block;\n\
    std::size_t front= 0;\n\
    std::size_t blockLength =0;\n\
    std::size_t maxElem = 0;\n\
    std::size_t items = 0;\n\
    std::mutex mtx;\n\
private:\n\
    //std::size_t blocksize = 8000; //how many elements fit in one block\n\
    //std::size_t blockLength; //amount of TypeT pointers per block\n\
\n\
\n\
\n\
    public:\n\
    void appendBlock(){\n\
        /*appending a new block*/\n\
        this->topblock=(this->topblock+1)%MAXINT;\n\
        this->blockList[this->topblock]=stackCreate(this->blocksize);\n\
    }\n\
    void removeBlock(){\n\
        /*removing the old block*/\n\
        delete[] this->blockList[currentblock];\n\
        this->blockList[currentblock] = nullptr;\n\
        this->currentblock = (this->currentblock+1) % MAXINT;\n\
\n\
    }\n\
    TypeT* stackCreate(std::size_t size)\n\
    {\n\
\n\
        TypeT* array = new TypeT[size];\n\
\n\
        return array;\n\
\n\
    }\n\
    public:\n\
        ramQueue(std::size_t n){\n\
            this->height=0;\n\
            this->blocksize = sizeof(TypeT) * n;\n\
            this->blockList = static_cast<TypeT** >(std::malloc( sizeof(TypeT*) * MAXINT ));\n\
            this->blockList[0]=stackCreate(this->blocksize);\n\
            this->blockLength = n;\n\
            this->maxElem= n * MAXINT;\n\
            \n\
            this->top = &this->blockList[0][0];\n\
            this->bottom =this->top;\n\
            for (int i = 1; i < MAXINT; i++) {\n\
                this->blockList[i] = nullptr;\n\
            }\n\
        }\n\
        ~ramQueue(){\n\
            for (int i = 0; i < MAXINT; i++) {\n\
                delete[] this->blockList[i];\n\
            }\n\
            std::free(this->blockList);\n\
        }\n\
    void enqueue(TypeT* ptr){\n\
    mtx.lock();\n\
\n\
        if(this->items==this->maxElem){\n\
            printf(\"Stack overflow\");\n\
            exit(1);\n\
        }\n\
        \n\
        if(this->height>0 && this->height%this->blockLength==0){\n\
            this->appendBlock();\n\
        }\n\
\n\
        this->blockList[topblock][this->height%this->blockLength]=*ptr;\n\
        this->top = &this->blockList[topblock][this->height % this->blockLength];\n\
        if (this->empty()) {\n\
            this->bottom = this->top;\n\
        }\n\
        this->height=(this->height+1) % this->maxElem;\n\
\n\
        this->items+=1;\n\
        \n\
    mtx.unlock();\n\
    }\n\
    void dequeue(){\n\
    mtx.lock();\n\
        \n\
        if(this->height==this->front)\n\
            return;\n\
\n\
        this->front=(this->front+1)%this->maxElem;\n\
\n\
\n\
        \n\
        if(this->front%this->blockLength==0){\n\
            this->removeBlock();\n\
        }\n\
        this->bottom=&this->blockList[currentblock][this->front%this->blockLength];\n\
\n\
    mtx.unlock();\n\
    }\n\
    TypeT* Top() {\n\
        if (!this->height) {\n\
            return nullptr;\n\
        }\n\
        return this->top;\n\
    }\n\
    TypeT* Bot() {\n\
        if (this->empty())\n\
            return nullptr;\n\
        return this->bottom;\n\
    }\n\
    bool empty() {\n\
        return this->height == this->front;\n\
    }\n\
\n\
};\n\
"};