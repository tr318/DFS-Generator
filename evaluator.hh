#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include "codeTemplate.hh"
using namespace std;

stringstream Iteratorbuffer;
struct Iterator {
    int start = 0;
    int end = 0;
    int step = 0;
    Iterator* upper = nullptr;
    Iterator* bound_next = nullptr; //in example for i which is bound to x
    Iterator* lower = nullptr; //j which is of lower rank to x, but has its own 'range' of definition
    string type;
    string name;
    bool inSCOPE = false;
    ~Iterator() {
        delete bound_next;
        delete lower;
    }
};
Iterator* getVariable(Iterator* param, string name){
    Iterator* head = param;
    //cout<<"\tgetVariable "<<name<<endl;
    Iterator* ptr_tmp = head;
    while(head!=nullptr){
        //cout<<"\t"<<head->name<<"\n";
        if(head->name==name)
            return head;
        ptr_tmp = head;

        while(ptr_tmp->bound_next != nullptr){
            //cout<<"\t ptr_tmp "<<ptr_tmp->bound_next->name<<"\n";
            ptr_tmp = ptr_tmp->bound_next;
            if(ptr_tmp->name==name)
                return ptr_tmp;
        }
        head=head->lower;

    }
    //cout<<"\tgetVariable";
    return nullptr;

}
void printVariable(Iterator* head){
        Iterator* ptr_tmp = head;
    while(head!=nullptr){
        cout<<head->name<<": "<<head->type;
        ptr_tmp = head;
        while(ptr_tmp->bound_next != nullptr){
            ptr_tmp = ptr_tmp->bound_next;
            cout<<", "<<ptr_tmp->name<<": "<<ptr_tmp->type<<" "<<ptr_tmp->end<<"\t";
        }
        head=head->lower;
        cout<<endl;
    }
    
}

void linker_dfs_bfs(
    Iterator* fptr,  //variable tree
    stringstream& usercodebuffer1, //code for before push
    stringstream& usercodebuffer2,  //code for after pop
    stringstream& singleNodeINGraphBuffer,
    stringstream& breakif,
    stringstream& memoryBuffer,
    stringstream& whiteBuffer,
    ofstream& nodeOutput,
    ofstream& dfsOutput,
    ofstream& bfsOutput,
    ofstream& ramStackOutput,
    ofstream& queueOutput,
    ofstream& cIteratorOutput,
    vector<string> includes,
    stringstream& searchinsertbuffer,
    stringstream& BreakIfbuffer
){
  Iterator* variable = fptr;
    string zuV;
    string vIteratorsRevert;
    string vIterators; //wird bei ersten Knoten v eingefügt 
    string iteratorPointers;
    string declarations;
    string integerDeclarations;
    string filler = "\n\n\n"; 
    string upper;
    string mupper;
    string szkVariables;
    string graphVariables;
    cout<<"Started Linking..."<<endl;
    

    if(memoryBuffer.str().size()){
        //std::cout<<memoryBuffer.str()<<endl;
        //exit(1);
        }
    
    while (variable!=nullptr) //1
    {   
        if(variable->type=="GRAPH"){
            graphVariables+="yyNode iterator_"+variable->name+";\n";
            graphVariables+="iterator_"+variable->name+"=v.n;\n";
        }
        if(variable->type=="RANGE"){
    
            iteratorPointers+="cIterator "+variable->name+";\n\t\t";
            vIterators+="\t\t\t"+variable->name+"= v."+variable->name+";\n";
            vIteratorsRevert +="\t\t\tv."+variable->name+"= "+variable->name+";\n";
            if(mupper.size()==0)
                mupper = variable->name; 
            if(upper.size()>0)
                vIterators+="\t\t\t"+upper+".lower=&"+variable->name+";\n\t\t";
            upper = variable->name; 
            
            declarations +="\tcIterator "+ variable->name+" =\n\t\t cIterator("+
            to_string(variable->start)+", "+
            to_string(variable->end)+", "+
            to_string(variable->step)+ 
            " "+");\n";
            integerDeclarations +="size_t iterator_"+variable->name+";\n";
            zuV += "\titerator_"+variable->name+"= "+variable->name+".val;\n\t";
        }
        if(variable->bound_next){
            variable=variable->bound_next;
        }
        else
            variable = variable->lower;
        
    }
    {
        ofstream of;
        of.open("yyGraphGenerator/yyIncludes.hh");
        for (string in:includes){
            of<<"#include \""<<in<<"\""<<endl;
        }
        of<<integerDeclarations<<endl;
        of.close();
    }
    nodeOutput<<"\n\n struct tuple{\n\t yyNode n;\n\t int seen;\n"<<declarations<<"};\n";
    //nodeOutput<<integerDeclarations;
    {
        cout<<"Writing BFS File..."<<endl;
        int index = 0;
        bfsOutput<<"#define MAXINT 10000\n";
        bfsOutput<<"//"<<index<<endl; 
        bfsOutput<<codeTemplateBFS[index]<<filler;
        
        index++;
        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index];
        bfsOutput<<iteratorPointers;
        if(memoryBuffer.str().size())
            bfsOutput<<memoryBuffer.str();
        bfsOutput<<filler;
        index++;
        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index];
        bfsOutput<<filler<<endl;

        index++;
        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index]<<filler;
        
        index++;
        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index]<<filler;
        
        index++;
        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index];
        bfsOutput<<vIterators;
        bfsOutput<<filler;
        if(singleNodeINGraphBuffer.str().size()){
            bfsOutput<<graphVariables;
            bfsOutput<<singleNodeINGraphBuffer.str()<<filler;
            }
        index++;


        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index];
       
        //cout<<"Breakif buffer: "<<BreakIfbuffer.str()<<endl;
        if(BreakIfbuffer.str().size())
            bfsOutput<<"("<<BreakIfbuffer.str()<<"&&";
        bfsOutput<<mupper+".next()";
        index++;


        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index];
        bfsOutput<<zuV;
        //bfsOutput<<vIterators<<endl;
        bfsOutput<<"w.n  = yyNode(&v.n, (long) -1 );\n"<<filler;
        if(searchinsertbuffer.str().size())
            bfsOutput<<searchinsertbuffer.str()<<filler;
        index++;
        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index]<<filler;
        
        if(whiteBuffer.str().size())
            bfsOutput<<"&&"<<whiteBuffer.str()<<endl;
        index++;
        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index]<<filler;
        

        index++;
        bfsOutput<<"//"<<index<<endl;
        if(usercodebuffer1.str().size())
            bfsOutput<<usercodebuffer1.str()<<filler;
        bfsOutput<<codeTemplateBFS[index]<<filler;

        index++;
        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index]<<filler;
        if(usercodebuffer2.str().size())
            bfsOutput<<usercodebuffer2.str()<<filler;
        index++;
        bfsOutput<<"//"<<index<<endl;
        bfsOutput<<codeTemplateBFS[index]<<filler;

        ofstream yyMain;
        yyMain.open("yyGraphGenerator/yyBFSMain.cc");
        yyMain<<"#include \"yyBFS.hh\"\n"<<codeTemplateMain[0];
        yyMain<<"yyNode FirstNode= yyNode();\n";
        yyMain<<"yyBFS(&FirstNode);\n";
        yyMain<<codeTemplateMain[1];
        yyMain.close();
    }
    {
        cout<<"Writing DFS File..."<<endl;
        int index = 0;
        dfsOutput<<"#define MAXINT 10000\n";
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index]<<filler;

        index++;
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index];
        dfsOutput<<iteratorPointers;
        if(memoryBuffer.str().size())
            dfsOutput<<memoryBuffer.str();
        dfsOutput<<filler<<endl;
        index++;
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index]<<filler;

        index++;
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index]<<filler;

        index++;
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index]<<filler;
        dfsOutput<<vIterators<<filler;
        if(singleNodeINGraphBuffer.str().size()){
            dfsOutput<<graphVariables;
            dfsOutput<<singleNodeINGraphBuffer.str()<<filler;
            }
        index++;
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index]<<filler;

        index++;
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index]<<filler;
        index++;
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index]<<filler;

        if(BreakIfbuffer.str().size())
            dfsOutput<<"("<<BreakIfbuffer.str()<<"&&";
        dfsOutput<<mupper+".next()";

        index++;
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index]<<filler;
        dfsOutput<<zuV;        

        index++;
        dfsOutput<<"//"<<index<<endl;        
        dfsOutput<<codeTemplateDFS[index]<<filler;
        if(whiteBuffer.str().size())
            dfsOutput<<"&&("<<whiteBuffer.str()<<endl;

        index++;
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index]<<filler;

        index++;
        dfsOutput<<"//"<<index<<endl;
        if(usercodebuffer1.str().size())
            dfsOutput<<usercodebuffer1.str()<<endl;
        dfsOutput<<vIteratorsRevert;
        dfsOutput<<codeTemplateDFS[index]<<filler;

        index++;
        if(usercodebuffer2.str().size())
            dfsOutput<<usercodebuffer2.str()<<endl;
        dfsOutput<<"//"<<index<<endl;
        dfsOutput<<codeTemplateDFS[index]<<filler;


        ofstream yyMain;
        yyMain.open("yyGraphGenerator/yyDFSMain.cc");
        yyMain<<"#include \"yyDFS.hh\"\n"<<codeTemplateMain[0];
        yyMain<<"yyNode FirstNode= yyNode();\n";
        yyMain<<"yyDFS(&FirstNode);\n";
        yyMain<<codeTemplateMain[1];
        yyMain.close();
    }


    ramStackOutput.open("yyGraphGenerator/ramStack.hh");
    ramStackOutput<<codeTemplateRamStack[0];
    ramStackOutput.close();
    queueOutput.open("yyGraphGenerator/ramQueue.hh");
    queueOutput<<codeTemplateQueue[0];
    queueOutput.close();
    cIteratorOutput.open("yyGraphGenerator/cIterator.hh");
    cIteratorOutput<<codeTemplateCIterator[0];
    cIteratorOutput.close();

}
void linker(
    Iterator* fptr,  //variable tree
    stringstream& usercodebuffer1, //code for before push
    stringstream& usercodebuffer2,  //code for after pop
    stringstream& szkCodeBuffer, //what we parse in the main thing
    stringstream& singleNodeInSZKCodeBuffer,
    stringstream& singleNodeINGraphBuffer,
    stringstream& breakif,
    stringstream& memoryBuffer,
    stringstream& whiteBuffer,
    ofstream& nodeOutput,
    ofstream& tarjanOutput,
    ofstream& ramStackOutput,
    ofstream& cIteratorOutput,
    vector<string> includes,
    stringstream& searchinsertbuffer,
    stringstream& BreakIfbuffer
     ){

         /*1 Parsing Iterators from Variable Tree
         * 2 Make a tuple struct in yyNode.hh
         * 3 Then start printing
         */ 
    Iterator* variable = fptr;
    string zuV;
    string vIteratorsRevert;
    string vIterators; //wird bei ersten Knoten v eingefügt 
    string iteratorPointers;
    string declarations;
    string integerDeclarations;
    string filler = "\n\n\n"; 
    string upper;
    string mupper;
    string szkVariables;
    string graphVariables;
    cout<<"Started Linking..."<<endl;

    while (variable!=nullptr) //1
    {   
        if(variable->type=="GRAPH"){
            graphVariables+="variable iterator_"+variable->name+";\n";
            graphVariables+="iterator_"+variable->name+".dfsNum=v.n.dfsNum;\n";
            graphVariables+="iterator_"+variable->name+".lowLink=v.n.lowLink;\n";
        }
        if(variable->type=="SZK"){
            szkVariables+="variable iterator_"+variable->name+";\n";
            szkVariables+="iterator_"+variable->name+".dfsNum=v.n.dfsNum;\n";
            szkVariables+="iterator_"+variable->name+".lowLink=v.n.lowLink;\n";
        }
        if(variable->type=="RANGE"){
    
            iteratorPointers+="cIterator "+variable->name+";\n\t\t";
            vIterators+="\t\t\t"+variable->name+"= v."+variable->name+";\n";
            vIteratorsRevert +="\t\t\tv."+variable->name+"= "+variable->name+";\n";
            if(mupper.size()==0)
                mupper = variable->name; 
            if(upper.size()>0)
                vIterators+="\t\t\t"+upper+".lower=&"+variable->name+";\n\t\t";
            upper = variable->name; 
            
            declarations +="\tcIterator "+ variable->name+" =\n\t\t cIterator("+
            to_string(variable->start)+", "+
            to_string(variable->end)+", "+
            to_string(variable->step)+ 
            " "+");\n";
            integerDeclarations +="size_t iterator_"+variable->name+";\n";
            zuV += "\titerator_"+variable->name+"= "+variable->name+".val;\n\t";
        }
        if(variable->bound_next){
            variable=variable->bound_next;
        }
        else
            variable = variable->lower;
        
    }
    //2
    nodeOutput<<"\n\n struct tuple{\n\t yyNode n;\n\t int seen;\n"<<declarations<<"};";
    nodeOutput<<"\n\n struct variable{\n\t size_t dfsNum;\n\t size_t lowLink;\n};"; 
    
    //3
    cout<<"Writing Tarjan File..."<<endl;
    int index = 0;
    tarjanOutput<<"#define MAXINT 1000\n"; 
    tarjanOutput<<codeTemplateTarjan[index]<<filler;
    if(memoryBuffer.str().size())
        tarjanOutput<<memoryBuffer.str()<<endl;
    //nodeOutput<<"\n"<<integerDeclarations<<endl;
    ofstream of;
    of.open("yyGraphGenerator/yyIncludes.hh");
    of<<"#pragma once\n";
    for (string in:includes){
        of<<"#include \""<<in<<"\""<<endl;
    }
    of<<integerDeclarations<<endl;
    of.close();


    index++;
    tarjanOutput<<codeTemplateTarjan[index];    
    tarjanOutput<<iteratorPointers<<filler;
    index++;

    tarjanOutput<<vIterators;
    //tarjanOutput<<mupper+".next();\n\t";
    tarjanOutput<<zuV;
    tarjanOutput<<"nextNode  = yyNode(&v.n, (long) next );\n";
    

    //Generate first neighbor

    //use declarations string 
    tarjanOutput<<codeTemplateTarjan[index]<<filler;
    tarjanOutput<<vIterators<<"//XDDDDDDDDDDDDDDDDDDDDDDDD"<<filler;
    index++;
    //tarjanOutput<<vIteratorsRevert;
    tarjanOutput<<codeTemplateTarjan[index];
    index++;
    

    //tarjanOutput<<breakif.rdbuf()<<"//BREAKIF" ;
    //breakif
    /*
    schauen welche Variablen im Konstruktor vorkommen.
    Danach der Reihenfolge durch iterieren.
    oder einfach so durch Iterieren ?? -> maybe User interaction mit Warning, but idk
    */
    //cout<<"BreakIfBuffer "<<BreakIfbuffer.str()<<endl;
    tarjanOutput<<"&&"<<mupper+".next()";
    //cout<<"Breakif buffer: "<<BreakIfbuffer.str()<<endl;
    if(BreakIfbuffer.str().size())
        tarjanOutput<<"&&("<<BreakIfbuffer.str()<<codeTemplateTarjan[index];
    else
        tarjanOutput<<"){"<<"\n";
    index++;    
    tarjanOutput<<"//Breakif + Iteration\n";
    tarjanOutput<<zuV<<endl;
    tarjanOutput<<"nextNode = yyNode(&v.n,(long) -1);";

    tarjanOutput<<codeTemplateTarjan[index]<<filler;
    index++;
    if(searchinsertbuffer.str().size())
        tarjanOutput<<searchinsertbuffer.str()<<filler;
    tarjanOutput<<codeTemplateTarjan[index]<<filler<<endl;
    index++;

    //cout<<"//breakif + Iteration";
    if(whiteBuffer.str().size())
    tarjanOutput<<"&&("<<whiteBuffer.rdbuf()<<filler;

    tarjanOutput<<codeTemplateTarjan[index]<<filler;
    index++;
    if(usercodebuffer1.str().size())
        tarjanOutput<<usercodebuffer1.str()<<"//Usercode 1"<<filler;

    tarjanOutput<<vIteratorsRevert<<endl;
    tarjanOutput<<codeTemplateTarjan[index]<<filler;
    index++;
    tarjanOutput<<codeTemplateTarjan[index]<<filler;
    if(usercodebuffer2.str().size())
        tarjanOutput<<usercodebuffer2.str()<<"//Usercode 2"<<filler;
    index++;

    tarjanOutput<<codeTemplateTarjan[index]<<filler;
    index++;
    
    string word;
    tarjanOutput<<szkVariables<<endl;
    while(szkCodeBuffer>> word)
    {
        //scope parsing
        tarjanOutput<<word;
    }
    tarjanOutput<<"\n"<<filler;

    tarjanOutput<<codeTemplateTarjan[index]<<filler;
    index++;
   
  
    /*
    Code für die einzelnde Komponente der SZKs
    
    */
    while(singleNodeInSZKCodeBuffer>> word)
    {
        //scope parsing
        if(word =="|»»»»" ){
            //cout<<"FOUND variable\n";

            singleNodeInSZKCodeBuffer>>word;
            variable = fptr;
            variable = getVariable(variable,word);
            //printVariable(fptr);
            //cout<<word<<" variable name:"<<variable->name<<"\n";
            variable->inSCOPE = true;
            //cout<<variable->name<<" "<<variable->type<<"\n";
            if(variable->type == "SZKNODE"){
                //cout<<"Is SZKNode\n"<<endl;
                string tmp = "\t\t\tyyNode iterator_"+variable->name+"=w.n;\n";
                tarjanOutput<<tmp;
            }
            if(variable->type == "RANGE"){
                //cout<<"Is RANGE\n"<<endl;
                string tmp = "\
                variable iterator_"+variable->name+";\n\
                iterator_"+variable->name+".dfsNum =w.n.dfsNum ;\n\
                iterator_"+variable->name+".lowLink =w.n.lowLink ;\n";
                tarjanOutput<<tmp;
            }
            continue;
        }
        if(word =="««««|" ){
            singleNodeInSZKCodeBuffer>>word;
            variable = getVariable(fptr,word);
            variable->inSCOPE = false;
            continue;
        }
        tarjanOutput<<word;
        
    }   tarjanOutput<<endl;
        tarjanOutput<<codeTemplateTarjan[index]<<filler;
        index++;
        tarjanOutput<<codeTemplateTarjan[index]<<filler;
        index++;
        //tarjanOutput<<usercodebuffer2.str()<<"//usercode2"<<filler;
        tarjanOutput<<codeTemplateTarjan[index]<<filler;
        index++;
        //tarjanOutput<<codeTemplateTarjan[index]<<filler;

        tarjanOutput<<"\n";
        ofstream yyMain;
        yyMain.open("yyGraphGenerator/yyMain.cc");
        yyMain<<"#include \"yyTarjan.hh\"\n"<<codeTemplateMain[0];
        yyMain<<"yyNode FirstNode= yyNode();\n";
        yyMain<<"yyTarjan(&FirstNode);\n";
        yyMain<<codeTemplateMain[1];
        yyMain.close();
        ramStackOutput.open("yyGraphGenerator/ramStack.hh");
        ramStackOutput<<codeTemplateRamStack[0];
        ramStackOutput.close();
        cIteratorOutput.open("yyGraphGenerator/cIterator.hh");
        cIteratorOutput<<codeTemplateCIterator[0];
        cIteratorOutput.close();

}