#pragma once

#include "graph.h"
#include <iostream>
#include "ramStack.h"
#include <list>

void printScc(std::list<adjNode*> scc) {
    std::cout << "Scc:";
    for (std::list<adjNode*>::iterator it = scc.begin(); it != scc.end(); it++) {
        std::cout << ", " << (*it)->cur;
    }
    std::cout << '\n';
}

// Basic DFS through the graph
void tarjan(DiaGraph* graph, int start) {
    ramStack<adjNode*> tarjanStack(3);	//stack for scc
    ramStack<adjNode*> backtrackStack(3);		//stack for backtracking
    // Initialize an empty dfs stack and push the "start" node into it
    ramStack<adjNode*> stack(3);
    adjNode* startelem = graph->head[start];
    stack.push(&startelem);

    int maxIndex = 0;	//set maxIndex = 0

    while (!stack.empty()) {
        adjNode* elem = *stack.Top();
        stack.pop();

        if (elem->mark == 1 || elem->mark == 2) {
            while (elem->prev != *backtrackStack.Top()) {
                adjNode* v = *backtrackStack.Top();
                v->mark = 2;
                backtrackStack.pop();
                (*backtrackStack.Top())->lowlink = (*backtrackStack.Top())->lowlink < v->lowlink ? (*backtrackStack.Top())->lowlink : v->lowlink;
                if (v->lowlink == v->index) { // If v is a root node, pop the stack and generate an SCC
                    std::list<adjNode*> scc;
                    adjNode* u;
                    do {
                        u = *tarjanStack.Top();
                        tarjanStack.pop();
                        u->onStack = false;
                        scc.push_back(u);
                    } while (u != v);
                    printScc(scc);
                }
            }
            if (elem->onStack) {	// if elem was seen the check if it is onStack css
                elem->prev->lowlink = elem->prev->lowlink < elem->index ? elem->prev->lowlink : elem->index; // if it is update lowlink if index of elem is smaller
            }
            continue;
        }

        // Mark the node as visited
        elem->mark = 1;
        if (backtrackStack.empty()) {
            elem->index = maxIndex;		// set index and lowlink to maxIndex
            elem->lowlink = maxIndex;
            maxIndex++;				// increment maxIndex
            tarjanStack.push(&elem);	// put vertex on scc
            elem->onStack = true;		// mark it as already on Stack
            backtrackStack.push(&elem);
        }
        else {
            while (elem->prev != *backtrackStack.Top()) {
                adjNode* v = *backtrackStack.Top();
                v->mark = 2;
                backtrackStack.pop();
                (*backtrackStack.Top())->lowlink = (*backtrackStack.Top())->lowlink < v->lowlink ? (*backtrackStack.Top())->lowlink : v->lowlink;
                if (v->lowlink == v->index) { // If v is a root node, pop the stack and generate an SCC
                    std::list<adjNode*> scc;
                    adjNode* u;
                    do {
                        u = *tarjanStack.Top();
                        tarjanStack.pop();
                        u->onStack = false;
                        scc.push_back(u);
                    } while (u != v);
                    printScc(scc);
                }
            }
            elem->index = maxIndex;		// set index and lowlink to maxIndex
            elem->lowlink = maxIndex;
            maxIndex++;
            tarjanStack.push(&elem);	// put it on the scc and mark it
            elem->onStack = true;
            backtrackStack.push(&elem);		// push elem on the backTrackStack to visit later again
        }
        std::cout << elem->cur << std::endl;

        // Push all following nodes to the stack that were not visited yet
        adjNode* nextPtr = elem;
        while (nextPtr != nullptr) {
            adjNode* nextNode = graph->head[nextPtr->val];
            stack.push(&nextNode);
            nextNode->prev = elem;

            nextPtr = nextPtr->next;
        }
    }

    // empty the backtrackStack

    while (!backtrackStack.empty()) {
        adjNode* v = *backtrackStack.Top();
        v->mark = 2;
        backtrackStack.pop();
        if (!backtrackStack.empty()) {
            (*backtrackStack.Top())->lowlink = (*backtrackStack.Top())->lowlink < v->lowlink ? (*backtrackStack.Top())->lowlink : v->lowlink;
        }
        if (v->lowlink == v->index) { // If v is a root node, pop the stack and generate an SCC
            std::list<adjNode*> scc;
            adjNode* u;
            do {
                u = *tarjanStack.Top();
                tarjanStack.pop();
                u->onStack = false;
                scc.push_back(u);
            } while (u != v);
            printScc(scc);
        }
    }
}