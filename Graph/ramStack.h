#include <cstddef>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#pragma once
#define MAXINT 10000



template <typename TypeT>
void stackDelete(TypeT array)
{

    std::free(array);
    //std::cout << "stack deleted" << std::endl;
}



template <typename TypeT>
class ramStack
{
    TypeT** blockList = nullptr;
    TypeT* bottom = nullptr;   //pointer to stack_bottom
    TypeT* top = nullptr;     //pointer to the top element (not array)
    std::size_t currentblock = 0; //shows which block of stack memory we are in
    std::size_t height = 0; //independently of of block size shows how many elements (Type*) are in stack
    std::size_t block;
private:
    std::size_t blocksize = 8000; //how many elements fit in one block
    std::size_t blockLength; //amount of TypeT pointers per block
    void appendBlock() {
        ///TO DO somehow implement a next pointer in the stack class
        //std::cout<<"mounting at this->blocklist["<<this->currentblock+1<<"]"<<std::endl;

        //vorher
        //this->blockList[currentblock++]=stackCreate(this->blocksize);



        //nachher
        this->currentblock++;
        this->blockList[currentblock] = stackCreate(this->blocksize);
    }
    void removeBlock() {
        delete[] this->blockList[currentblock];
        this->blockList[currentblock] = nullptr;
        this->currentblock--;

    }

    TypeT* stackCreate(std::size_t size)
    {
        //std::cout<<"Allocation this much memory: "<<size<<std::endl;
        TypeT* array = new TypeT[size];
        /*for(size_t i = 0;i<size;i+=sizeof(TypeT*))
            std::cout<<&array[i]<<std::endl;*/
        return array;

    }
public:
    ramStack(std::size_t n) {
        this->height = 0;
        this->blockList = static_cast<TypeT**>(std::malloc(sizeof(TypeT*) * MAXINT));
        this->blockList[0] = stackCreate(blocksize);
        this->blockLength = n;
        this->blocksize = sizeof(TypeT) * n;

        this->top = &this->blockList[0][0];
        this->blockLength = this->blocksize / sizeof(TypeT);

    }
    ~ramStack() {
        std::cout << "delete ramStack";

        delete[] this->blockList;
        /*  while (this->currentblock)
          {
              std::cout<<this->currentblock;
              delete this->blockList[currentblock];
              std::cout<<"success\n";
              currentblock--;
          }
          delete this->blockList[0];

          std::cout<<"Error";
          std::free( this->blockList);
    */
    }
    void push(TypeT* ptr) {

        //std::cout<<"height, currentblock, sizeofblock "<<this->height<<", "<<this->currentblock<<std::endl;
        if (this->height > 0 && this->height % this->blockLength == 0) {
            this->appendBlock();
        }

        //std::cout<<"Allocating at: "<<this->currentblock<<" "<<this->height%this->blockLength<<ptr<<"\n";

        this->blockList[currentblock][this->height % this->blockLength] = *ptr;
        this->top = &this->blockList[currentblock][this->height % this->blockLength];
        this->height++;
        //printf("still running\n");
        //std::cout<<sizeof((this->blockList[currentblock]+this->height))<<" "<<(this->blockList[currentblock]+this->height)<<std::endl;
    }
    void pop() {


        //this->blockList[currentblock][this->height%this->blockLength]=nullptr; // compile error, type covnersion int -> int*
        if (this->height == 0)
            return;

        if ((this->height - 1) % this->blockLength == 0 && currentblock != 0) { // height - 1 instead of height and added currentblock != 0
            this->removeBlock();
            this->height--;
            this->top = &this->blockList[currentblock][this->blockLength - 1]; //avoiding this one modulo operation // changed blocksize to blockLength
            return;
        }
        this->height--;
        this->top = &blockList[currentblock][(this->height - 1) % this->blockLength]; // height - 1 instead of height
    }
    TypeT* Top() {
        if (!this->height) {
            return nullptr;
        }
        return this->top;
    }
    TypeT* Bot() {
        if (this->empty())
            return nullptr;
        return this->bottom;
    }
    bool empty() {
        return this->height == 0;
    }
    void printStack(size_t a) {

        while (a)
        {
            printf("%zd\n", this->blockList[this->currentblock][a]);
            //std::cout<<this->blockList[this->currentblock][a]<<std::endl;
            a--;
        }
        printf("%zd\n", this->blockList[this->currentblock][0]);
        //std::cout<<this->blockList[this->currentblock][0]<<std::endl;

    }
    void printCurrentStack() {
        printStack(this->currentblock);
    }
    void dumpStackInfo() {
        size_t a = this->currentblock;
        std::cout << "Height, Blocks, BlockLength\n";
        std::cout << this->height << "|" << this->currentblock << "|" << this->blockLength << "\n";
        while (a)
        {
            printStack(a);
            a--;
            std::cout << "-----------------------------------------------------\n";
        }

    }
    void freeME() {
        //this->top=nullptr;
        //this->bottom=nullptr;
        std::cout << "this";
        while (this->currentblock)
        {
            std::cout << this->currentblock;
            free(this->blockList[currentblock]);
            std::cout << "success\n";
            currentblock--;
        }
        free(this->blockList[0]);

        std::cout << "Error";
        std::free(this->blockList);
    }
};
