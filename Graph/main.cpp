#include "graph.h"
#include "bfs.h"


// graph implementation
int main()
{
    // graph edges array.
    graphEdge edges[] = {
        // (x, y, w) -> edge from x to y with weight w
        {0,1,2},{0,2,4},{1,4,3},{2,3,2},{3,1,4},{4,3,3}
    };
    int N = 6;      // Number of vertices in the graph

    // calculate number of edges
    int n = sizeof(edges) / sizeof(edges[0]);

    // construct graph
    DiaGraph diagraph(edges, n, N);

    // print adjacency list representation of graph
    std::cout << "Graph adjacency list " << std::endl << "(start_vertex, end_vertex, weight):" << std::endl;
    for (int i = 0; i < N; i++)
    {
        // display adjacent vertices of vertex i
        display_AdjList(diagraph.head[i], i);
    }

    bfs(&diagraph, 0);

    // test for BFS. Only use one test at a time, because it changes every child of a node to seen.
    //dfs(&diagraph, diagraph.head[1], 4); // route from 1 -> 4 exists
    //bfs(diagraph, diagraph.head[0], 4); // route from 0 -> 4 exists
    //bfs(diagraph, diagraph.head[1], 0); // route from 1 -> 0 does'nt exists

    return 0;
}
