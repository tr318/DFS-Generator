#pragma once

#include "graph.h"
#include "ramStack.h"

struct tuple {
    adjNode* v;
    //cIterator it;
};

void dfs(DiaGraph* graph, int start) {
    // Initialize an empty dfs stack and push the "start" node into it
    ramStack<tuple> stack(3);
        tuple vTuple;
        vTuple.v = graph->head[start];//get first Node
        //vTuple.it =nullptr ;
    stack.push(&vTuple);

    // Decleare variables for the loop
    adjNode* elem, * nextPtr, * nextNode;

    while (!stack.empty()) {
        loop_begin:
        elem = stack.Top()->v;
        

        if (    elem->mark == 1) {
            continue;
        }

        // Mark the node as visited
        elem->mark = 1;
        std::cout << elem->cur << std::endl;

        // Push all following nodes to the stack that were not visited yet
        nextPtr = elem;
        while (nextPtr != nullptr) {
            nextNode = graph->head[nextPtr->val];   // successor generator here

            if (    nextNode->mark != 1) {  // search and insert here
                    tuple v_tmp;
                    v_tmp.v = nextNode;
                    //v_tmp.it = nullptr;


                    
                stack.push(&v_tmp);
                goto loop_begin;


                
            }

            nextPtr = nextPtr->next;
        } //man findet keinen Nachbarn, pop den Knoten weg
        stack.pop();
    }
}