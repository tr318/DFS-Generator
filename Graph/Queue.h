#define MAXINT 10000
#include <cstddef>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#pragma once

template <typename TypeT>
class ramQueue{
    TypeT** blockList = nullptr;
    TypeT* bottom = nullptr;   //pointer to stack_bottom
    TypeT* top = nullptr;     //pointer to the top element (not array)
    std::size_t blocksize=0;
    std::size_t currentblock = 0; //shows which block of stack memory we are in
    std::size_t topblock = 0;
    std::size_t height = 0; //independently of of block size shows how many elements (Type*) are in stack
    std::size_t block;
    std::size_t front= 0;
    std::size_t blockLength =0;
    std::size_t maxElem = 0;
    std::size_t items = 0;
private:
    //std::size_t blocksize = 8000; //how many elements fit in one block
    //std::size_t blockLength; //amount of TypeT pointers per block



    public:
    void appendBlock(){
        /*appending a new block*/
        this->topblock=(this->topblock+1)%MAXINT;
        this->blockList[this->topblock]=stackCreate(this->blocksize);
    }
    void removeBlock(){
        /*removing the old block*/
        delete[] this->blockList[currentblock];
        this->blockList[currentblock] = nullptr;
        this->currentblock = (this->currentblock+1) % MAXINT;

    }
    TypeT* stackCreate(std::size_t size)
    {

        TypeT* array = new TypeT[size];

        return array;

    }
    public:
        ramQueue(std::size_t n){
        this->height=0;
        this->blocksize = sizeof(TypeT) * n;
        this->blockList = static_cast<TypeT** >(std::malloc( sizeof(TypeT*) * MAXINT ));
        this->blockList[0]=stackCreate(this->blocksize);
        this->blockLength = n;
        this->maxElem= n * MAXINT;
        
        this->top = &this->blockList[0][0];
        this->bottom =this->top;

        for (int i = 1; i < MAXINT; i++) {
               this->blockList[i] = nullptr;
        }
        }
        ~ramQueue(){
            for (int i = 0; i < MAXINT; i++) {
                if(this->blockList[i] != nullptr)
                    delete[] this->blockList[i];
            }
            std::free(this->blockList);
        }
    void enqueue(TypeT* ptr){

        if(this->items==this->maxElem){
            printf("Stack overflow");
            exit(1);
        }
        
        if(this->height>0 && this->height%this->blockLength==0){
            this->appendBlock();
        }

        this->blockList[topblock][this->height%this->blockLength]=*ptr;
        this->top = &this->blockList[topblock][this->height % this->blockLength];
        if (this->empty()) {
            this->bottom = this->top;
        }
        this->height=(this->height+1) % this->maxElem;

        this->items+=1;
        
    }
    void dequeue(){
        
        if(this->height==this->front)
            return;

        this->front=(this->front+1)%this->maxElem;


        
        if(this->front%this->blockLength==0){
            this->removeBlock();
        }
        this->bottom=&this->blockList[currentblock][this->front%this->blockLength];

    }
    TypeT* Top() {
        if (!this->height) {
            return nullptr;
        }
        return this->top;
    }
    TypeT* Bot() {
        if (this->empty())
            return nullptr;
        return this->bottom;
    }
    bool empty() {
        return this->height == this->front;
    }

};
