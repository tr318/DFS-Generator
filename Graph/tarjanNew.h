#pragma once

#include "graph.h"
#include <iostream>
#include "ramStack.h"
#include <list>
#include <vector>

void printScc(std::list<adjNode*> scc) {
	std::cout << "Scc:";
	for (std::list<adjNode*>::iterator it = scc.begin(); it != scc.end(); it++) {
		std::cout << ", " << (*it)->cur;
	}
	std::cout << '\n';
}

struct onStackLookup {
	int index_start, index_end;
	bool* table;
	onStackLookup* next = nullptr;
};

onStackLookup osLookup;

bool isOnStack(int dfsNum) {
	onStackLookup* ptr = &osLookup;
	while (dfsNum > ptr->index_end)
	{
		ptr = ptr->next;
	}
	if (ptr->index_start == 0) {
		return ptr->table[dfsNum] == 1;
	}
	return ptr->table[dfsNum % ptr->index_start] == 1;
}

void addDfsNum(int dfsNum, bool val) {
	onStackLookup* ptr = &osLookup;
	int i = 0;
	while (dfsNum > ptr->index_end)
	{
		if (ptr == nullptr || ptr->index_start < 0 || ptr->index_end < 0) {
			std::cerr << "pointer is null" << std::endl;
			exit(1);
		}
		if (ptr->next == nullptr) {
			ptr->next = (onStackLookup*)std::malloc(sizeof(onStackLookup));
			onStackLookup lookUp;
			ptr->next->index_start = ptr->index_end + 1;
			ptr->next->index_end = ptr->index_end + MAXINT;
			//ptr->next->table = new bool[MAXINT];
			ptr->next->table = (bool*)std::calloc(MAXINT, sizeof(bool));
			ptr->next->next = nullptr;
			if (lookUp.table == nullptr || lookUp.table == NULL) {
				std::cerr << "could not allocate memory" << std::endl;
				exit(1);
			}
		}
		ptr = ptr->next;
		i++;
	}
	if (ptr->index_start == 0) {
		ptr->table[dfsNum] = val;
		//std::cout << dfsNum << std::endl;
		return;
	}
	//std::cout << dfsNum % ptr->index_start << std::endl;
	ptr->table[dfsNum % ptr->index_start] = val;
}

void test() {
	for (int i = 0; i < 20000; i++) {
		addDfsNum(i, true);
	}
	for (int i = 0; i < 20000; i++) {
		std::cout << isOnStack(i) << std::endl;
	}
}



struct tuple {
	adjNode* n;
};

struct viTuple {
	tuple t;
	int i;
};

void tarjan(DiaGraph* graph, int start) {
	osLookup.index_start = 0;
	osLookup.index_end = MAXINT - 1;
	osLookup.next = nullptr;
	osLookup.table = (bool*)std::malloc(MAXINT * sizeof(bool));
	int next = 0; // next index
	ramStack<tuple> stack(6);
	ramStack<viTuple> work(3); //Recursion stack
	tuple v;
	tuple w;
	int i;
	int j;
	bool recurse;
	tuple vi;
	vi.v = graph->head[start];
	vi.i = 0;
	work.push(&vi);
	while (!work.empty()) {
		v = work.Top()->t;
		i = work.Top()->i; // i is next successor to process
		work.pop();
		if (i == 0) { //When first visiting a Vertex
			v.n->dfsNum = next;
			//v.n->lowlink = next;

					setLowlink(next, next);

			stack.push(&v);
			v.n->onStack = true;
			addDfsNum(next, true);
			next++;
		}
		recurse = false;

		adjNode* nextPtr = v;
		j = 0;
		while (nextPtr != nullptr) {
			/*
				execute user generated code here


			*/
			//successor method
			w = graph->head[nextPtr->val];
			if (w->dfsNum == -1) { // search-insert here
				//Add w to recursion stack.
				viTuple v_tmp;
				v_tmp.t = v; 
				v_tmp.i = j + 1;
				work.push(&v_tmp);
				viTuple v_tmp2;
				v_tmp2.t = w;
				v_tmp2.i = 0;
				work.push(&v_tmp2);
				recurse = true;
				break;
			}
			else if (w->onStack && isOnStack(w->dfsNum)) {
				v->lowlink = v->lowlink < w->dfsNum ? v->lowlink : w->dfsNum;
			}
			j++;
			nextPtr = nextPtr->next;
		}
	//2nd user code after all dfs
		if (recurse) continue;
		if (v.n->dfsNum == 	getLowlink(v->dfsNum)/*v.n->lowlink*/) {
			// pop loop
			std::list<adjNode*> scc;
			while (true) {
				w = stack.Top();


				//w->onStack = false;
				addDfsNum(w->dfsNum, false);
				scc.push_back(w);

				if (w->dfsNum == v->dfsNum) {
					stack.pop(); //because v must be poped aswell, 
					break;
				}
				stack.pop(); // remove w's reference from stack 
			}
			printScc(scc);
		}

		if (!work.empty()) {
			w = v;
			v = work.Top()->t;

					setLowlink(v->dfsNum, getLowlink(v->dfsNum) < getLowlink(w->dfsNum) ? getLowlink(v->dfsNum) : getLowlink(w->dfsNum));
			//v.n->lowlink = v.n->lowlink < w.n->lowlink ? v.n->lowlink : w.n->lowlink;
		}
	}
}
