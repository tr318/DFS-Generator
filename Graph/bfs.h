#include "graph.h"
#include "Queue.h"

struct tuple {
    adjNode* v;
    //cIterator it;
};

//search through whole graph
void bfs(DiaGraph* graph, int start) {
    int next = 0;
    Queue<tuple> queue(5);
    tuple vTuple;
        vTuple.v = graph->head[start];//get first Node
        //vTuple.it = ;
    queue.enqueue(&vTuple);

    graph->head[start]->mark = 1;

    adjNode* node, *nextNode;

    while (!queue.empty()) {

        node = queue.Bot()->v;
        node->dfsNum = next;
        next++;
        node->mark = 2;
        std::cout << node->cur << std::endl;

        while (node != nullptr) { // while node got children
				nextNode = graph->head[node->val];// 	successor generator here
            if (    nextNode->mark != 2) { // 	Search and insert here
                    tuple v_tmp;
                    v_tmp.v = nextNode;
                    //v_tmp.it =nullptr;

                queue.enqueue(&v_tmp);
                nextNode->mark = 1;
            }
            node = node->next; // put next header on next children
        }
        
        queue.dequeue();
    }
}
/*
/// Breadth-first search
/// \param graph graph in format from graph.h
/// \param startNode start node form adjacency list 
/// \param goalValue value (id) from goal node
/// \return true of route from \a startNode to \a goalNode exists, false else
bool bfs(DiaGraph* graph, adjNode* startNode, int goalValue) {

    Queue<tuple> queue(5);
    tuple vTuple;
        vTuple.v = startNode; //get first Node
        vTuple.it = ;
    queue.enqueue(&vTuple);

    startNode->mark = 1; // set startNode as seen

    adjNode* node;
    adjNode* nextNode;

    while (!queue.empty()) { 

        std::cout << "in loop 1 (deque) " << std::endl;

        node = queue.Bot()->v;
        queue.dequeue(); // delete first element of deque

        node->mark = 2; // mark node as visited

        if (node->val == goalValue) {

            std::cout << "Route exists" << std::endl;
            std::cout << std::endl;
            return true;
        }

        nextNode = graph->head[node->val];

        while (node != nullptr) { // while node got children
            nextNode = graph->head[node->val];
            if (nextNode->mark != 2) { // if child is not marked as visited
                    tuple v_tmp;
                    v_tmp.v = nextNode;
                    v_tmp.it = ;
                queue.enqueue(&v_tmp);
                nextNode->mark = 1; // mark child as seen
            }
            node = node->next; // put next header on next children
        }
        
    }
    
    std::cout << "Route does'nt exists" << std::endl;
    std::cout << std::endl;

    return false;
}
*/
