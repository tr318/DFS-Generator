#include <string>

 class cIterator
{
public:

    std::string id;
    long val;
    long end;
    long step;
    cIterator* lower=nullptr;

    int next()  {
        if(lower && lower->next()==true)
            return true;
        if (this->val==end)
            return false;
        this->val += 1 ; 
        return true;
    }
    long getValue(){
        return this->val; 
    }
    cIterator(){

    }
    cIterator(long end, std::string id){
        //*(this->id) = id;     
        this->end = end;
        this->val = 0;
        this->step = 1;
        //this->getValue = getValue;

    }
    cIterator(  int val, long end, std::string id){
        //*(this->id) = id;     
        this->end = end;
        this->val = val;
        this->step = 1;
        //this->getValue = getValue;

    }
    cIterator(  int val, long end, std::string id, long step){
        //*(this->id) = id;     
        this->end = end;
        this->val = val;
        this->step= step;
        //this->getValue = getValue;

    }

    ~cIterator(){

    }

};
/*std::ostream& operator<<(std::ostream &strm, cIterator it){
        return strm <<" id "<<it.id <<" value "<<it.getValue()<<" end "<<it.end;
    }*/