#pragma once
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <algorithm>
using namespace std;

stringstream whiteBuffer;
vector<string> Includ = {};
vector<string> keywords = { "Neighbor", "Node","DfsNum","LowLink" };

stringstream memoryBuffer;
stringstream searchinsertbuffer;
stringstream BreakifBuffer;
bool SI = false;
string parsekeyword(string str) {
	//count $$, if 0 return str, else replace $k$ with iterator_k (regardless of memory error :))
	   //count $$, if 0 return str, else replace $k$ with iterator_k (regardless of memory error :))
	start:
	int found = str.find("$", 0);
	vector<size_t> v;

	v.empty();
	v.push_back(found);
	int count = 0;
	while (found != string::npos)
	{
		count++;
		found = str.find("$", found + 1);
		v.push_back(found);
	}
	v.pop_back();
	if (count == 0)
		return str;
	if (count % 2 == 1) {
		return str;
	}

	for (int i = 0, j = 1; i < v.size(); i += 2, j += 2)

	{	string sub;
		char c = str[v[i]];
		char d = str[v[j]];
		for (int x = v[i] + 1; x < v[j]; x++)
			sub += str[x];
		string a = str.substr(0, v[i]);
		//cout<<str<<" "<<v[i]<<" "<<v[j]<<" "<<str.size()<<endl;
		//v[j]= v[j]>str.size() ? str.size() : v[j];
		string b;
		if(v[j]>=str.size())
			b = ""; 
		else
			b = str.substr(v[j]+1, string::npos);
		//cout<<"showing it:"<<a << " " <<sub<< " "<<b<< endl;
		if (sub == keywords[0]) {
			// I have to think how to implement the constructor into the program
			//Neighbor should be a pointer to the Node on top of this main one
			//name of the main one should be 'v' in our tarjan implementation
			//it's w for now
			if (str[v[j] + 1] == '.'){
			if(b!="") b= b.substr(1,string::npos);
				str = a + "w.n." + b;
				} //correct to pointer notation
			else
				str = a + "w.n" + b;
		}
		else if (sub == keywords[1]) { 
			if (str[v[j] + 1] == '.'){
				if(b!="")b= b.substr(1,string::npos);
				str = a + "v.n." + b;
				} //correct to pointer notation
			else
				str = a + "v.n" + b;//v in Tarjan
		}
		else if (sub == keywords[2]) {
			str = a + "dfsNum" + b;
		}
		else if (sub == keywords[3]) {
			str = a + "lowlink" + b;
		}
		else {
			//str.replace(v[i],v[j],"iterator_"+sub.substr(1,sub.size()));
			string a = str.substr(0, v[i]);
			string b;
			if(v[j]>=str.size())
				b = ""; 
			else
				b = str.substr(v[j]+1, string::npos);
			//cout << c << " " << d << i << " " << j << sub << " a: " << a << " b: " << b << endl;
			str = a + "iterator_" + sub + b;
			
		}
		if(count >1)
			goto start;

	}
	return str;
}
void parseBreakif(ifstream& infile){
	cout << "Parse Breakif..."<<endl;
	string word;
	string expr1;
	bool next = false;
	//infile >> word;
	//if (word != "{") std::cerr << "Syntaxerror in White function" << std::endl;
	//whiteBuffer << "if(\n";
	string tmp;
	int count = 0;
	while (infile >> word) {
		//filter comments
		size_t foundComment = word.find("//");
		if (foundComment <= word.length() - 1) {
			getline(infile, word);
			word = word.substr(0, foundComment - 1);
		}


		if (next && word != "}") {
			if (expr1 != "") {
				if (count == 0) {
					expr1 = "";
					next = false;
					continue;
				}
				expr1 = parsekeyword(expr1);
				BreakifBuffer << "(" << expr1 << ") && \n";
				expr1 = "";
				next = false;
			}
		}
		else {
			if (next) {
				if (count == 0) {
					expr1 = "";
					next = false;
					BreakifBuffer << expr1 << "1\n";//return 1; \n else return 0;";
					break;
				}
				expr1 = parsekeyword(expr1);
				BreakifBuffer << expr1 << ")\n";//;return 1; \n else return 0;";
				break;
			}
			if (word == "}" && !next) {
				BreakifBuffer.str("");
				//whiteBuffer << "if(1) 1;\nelse 0";
				break;
			}
		}

		//findet semicolon
		size_t found = word.find(";");
		if (found < word.length() - 1) {
			std::cerr << "Syntaxerror in White function" << std::endl;
		};
		// if semicolon found and word no empty
		if (found == word.length() - 1 && word != "") {
			word = word.substr(0, found);
			// expression complete
			next = true;
			expr1.append(word);
			int depth = 0;
			count = 0;
			for (int j = 0; j < expr1.size(); j++) {
				if (expr1[j] == '(') {
					depth++;
					count++;
				}
				else if (expr1[j] == ')') depth--;
				if (depth < 0) {
					cerr << "Syntaxerror did not expect \')\'";
				}
			}
			if (depth > 0) {
				cerr << "Syntaxerror expected \')\'";
			}
			continue;
		}
		//parsed keyw�rter und variablen sonst
		else {

		}


		expr1.append(word);

	}
}
	
void parseSearchInsert(stringstream& line){
	//long dfsNum = -1;
	cout<<"Parsing SearchInsert...\n";
	string word; 
	line>>word;
	SI = true;
	if(word!= "="){
		//cout<<word<<";\n";
		cerr<<"Error: Expected Single '=' after Searchinsert\n";
		exit(1);
	}
	searchinsertbuffer<<"long yySI =";
	while(line>>word)
		searchinsertbuffer<<parsekeyword(word)<<"\n";
	searchinsertbuffer<<"w.n.dfsNum = yySI;\n";
	//cout<<searchinsertbuffer.str();
}

void parseWhite(ifstream& infile) {
	string word;
	string expr1;
	cout<<"Parsing White...\n";
	bool next = false;
	//infile >> word;
	//if (word != "{") std::cerr << "Syntaxerror in White function" << std::endl;
	//whiteBuffer << "if(\n";
	string tmp;
	int count = 0;
	while (infile >> word) {
		//filter comments
		size_t foundComment = word.find("//");
		if (foundComment <= word.length() - 1) {
			getline(infile, word);
			word = word.substr(0, foundComment - 1);
		}


		if (next && word != "}") {
			if (expr1 != "") {
				if (count == 0) {
					expr1 = "";
					next = false;
					continue;
				}
				expr1 = parsekeyword(expr1);
				whiteBuffer << "(" << expr1 << ") && \n";
				expr1 = "";
				next = false;
			}
		}
		else {
			if (next) {
				if (count == 0) {
					expr1 = "";
					next = false;
					whiteBuffer << expr1 << "1\n";//return 1; \n else return 0;";
					break;
				}
				expr1 = parsekeyword(expr1);
				whiteBuffer << expr1 << ")\n";//;return 1; \n else return 0;";
				break;
			}
			if (word == "}" && !next) {
				whiteBuffer.str("");
				//whiteBuffer << "if(1) 1;\nelse 0";
				break;
			}
		}

		//findet semicolon
		size_t found = word.find(";");
		if (found < word.length() - 1) {
			std::cerr << "Syntaxerror in White function" << std::endl;
		};
		// if semicolon found and word no empty
		if (found == word.length() - 1 && word != "") {
			word = word.substr(0, found);
			// expression complete
			next = true;
			expr1.append(word);
			int depth = 0;
			count = 0;
			for (int j = 0; j < expr1.size(); j++) {
				if (expr1[j] == '(') {
					depth++;
					count++;
				}
				else if (expr1[j] == ')') depth--;
				if (depth < 0) {
					cerr << "Syntaxerror did not expect \')\'";
				}
			}
			if (depth > 0) {
				cerr << "Syntaxerror expected \')\'";
			}
			continue;
		}
		//parsed keyw�rter und variablen sonst
		else {

		}


		expr1.append(word);

	}
	
}
void parseMemory(ifstream& infile){

	cout<<"Parsing Memory...\n";
	string word;
	//infile >> word;
	bool blockSize= false;
	bool ItemsPerBlock= false;
	//if(word != "="){
		//Syntaxerror
	//}
	//infile >> word;
	//if(word[0]!='{'){
		//Syntaxerror
	//}
	while (infile >> word)
	{	
		if(word[0]=='/'&&word[1]=='/'){ //one line comment
			getline(infile,word);
		} 
		if(word=="BlockSize:"){
			infile >> word;
			
			memoryBuffer<<"size_t BlockSize = "<<word<<";\n";
			blockSize=true;
		}
		if(word=="ItemsPerBlock:"){
			infile >> word;
			
			memoryBuffer<<"size_t ItemsPerBlock = "<<word<<";\n";
			ItemsPerBlock=true;
		}
		if(word=="}"){
			break;
		}
	}
	if(blockSize&&!ItemsPerBlock){
		memoryBuffer<<"size_t ItemsPerBlock = BlockSize/sizeof(tuple);\n";
	}
	if(blockSize && ItemsPerBlock){
		memoryBuffer<<"ItemsPerBlock = (BlockSize/sizeof(tuple)) < ItemsPerBlock?(BlockSize/sizeof(tuple)) : ItemsPerBlock;\n";
	}
	if(!blockSize && ItemsPerBlock){
		//Default Value
		memoryBuffer<<"size_t BlockSize = sizeof(yyNode)*ItemsPerBlock;\n";
	}
	if(!blockSize && !ItemsPerBlock){
		//Default Value
		memoryBuffer<<"size_t ItemsPerBlock = 1;\n";
		memoryBuffer<<"size_t BlockSize = sizeof(yyNode)*ItemsPerBlock;\n";
	}
	memoryBuffer<<"BoolsPerBlock = BlockSize/sizeof(bool);\n";
	memoryBuffer<<"LongsPerBlock = BlockSize/sizeof(long);\n";
	return;
}
void parseIncludes(ifstream& infile){
	cout<<"Parsing Includes..."<<endl;
	string word;
	while(infile >> word){
		if (word=="}")
		{
			return;
			
		}
		
		Includ.push_back(word);
	}

}