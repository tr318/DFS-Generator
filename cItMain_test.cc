#include "cIterator.hh"
#include "iostream"
#include "string"
using namespace std;
struct thing{
    cIterator i;
    cIterator k;
};
int main()
{   
    string a; 
    cout<<"heh?"<<std::endl;
    cIterator it =  cIterator (10,20,"it");
    cout<<"heh?"<<std::endl;
    thing* th = new thing;
    th->k = cIterator(1,6,a+ "k");
    th->k.lower = &it;
    th->i =cIterator(11, 20, a+ "i");
    cout<<" id "<<th->i.id <<" value "<<th->i.getValue()<<" end "<<th->i.end<<std::endl; // 11
    th->i.next();
    it = th->i;
    cout<<" id "<<it.id <<" value "<<it.getValue()<<" end "<<it.end<<std::endl; //12
    th->i.next();
    th->k.next();
    thing TH;
    cout<<" id "<<it.id <<" value "<<it.getValue()<<" end "<<it.end<<std::endl; //val should be 13
    cout<<" id "<<th->i.id <<" value "<<th->i.getValue()<<" end "<<th->i.end<<std::endl; // 13 
    return 0;
}
