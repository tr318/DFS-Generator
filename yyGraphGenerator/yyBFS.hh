#define MAXINT 10000
//0
#pragma once
#include "ramQueue.hh"
#include "yyNode.hh"
#include <iostream>
size_t LongsPerBlock;
size_t BoolsPerBlock;
struct onStackLookup {
	int index_start, index_end;
	bool* table;
	onStackLookup* next = nullptr;
	~onStackLookup(){
		std::cout << "Destructing OnStackLookUp...\n";
		std::free(table);
	}};

onStackLookup osLookup;

bool isOnStack(int dfsNum) {
	onStackLookup* ptr = &osLookup;
	while (dfsNum > ptr->index_end)
	{
		ptr = ptr->next;
	}
	if (ptr->index_start == 0) {
		return ptr->table[dfsNum] == 1;
	}
	return ptr->table[dfsNum % ptr->index_start] == 1;
}

void addDfsNum(int dfsNum, bool val) {
	onStackLookup* ptr = &osLookup;
	int seen = 0;
	while (dfsNum > ptr->index_end)
	{
		if (ptr == nullptr || ptr->index_start < 0 || ptr->index_end < 0) {
			std::cerr << "pointer is null" << std::endl;
			exit(1);
		}
		if (ptr->next == nullptr) {
			ptr->next = (onStackLookup*)std::malloc(sizeof(onStackLookup));
			onStackLookup lookUp;
			ptr->next->index_start = ptr->index_end + 1;
			ptr->next->index_end = ptr->index_end + BoolsPerBlock;
			//ptr->next->table = new bool[MAXINT];
			ptr->next->table = (bool*)std::calloc(BoolsPerBlock, sizeof(bool));
			ptr->next->next = nullptr;
			if (lookUp.table == nullptr || lookUp.table == NULL) {
				std::cerr << "could not allocate memory" << std::endl;
				exit(1);
			}
		}
		ptr = ptr->next;
		seen++;
	}
	if (ptr->index_start == 0) {
		ptr->table[dfsNum] = val;
		//std::cout << dfsNum << std::endl;
		return;
	}
	//std::cout << dfsNum % ptr->index_start << std::endl;
	ptr->table[dfsNum % ptr->index_start] = val;
}

void freeOsLookup() {



//1
onStackLookup* osTmp = &osLookup;
    onStackLookup* osTmp2;
    std::free(osTmp->table);
    osTmp = osTmp->next;
    while (osTmp != nullptr) {
        osTmp2 = osTmp;
        osTmp = osTmp2->next;
        std::free(osTmp2->table);
    }
}
    void yyBFS(yyNode* firstNode) {
cIterator i;
		size_t BlockSize = 10000;
size_t ItemsPerBlock = 20;
ItemsPerBlock = (BlockSize/sizeof(tuple)) < ItemsPerBlock?(BlockSize/sizeof(tuple)) : ItemsPerBlock;
BoolsPerBlock = BlockSize/sizeof(bool);
LongsPerBlock = BlockSize/sizeof(long);



//2
osLookup.index_start = 0;
	osLookup.index_end = BoolsPerBlock - 1;
	osLookup.next = nullptr;
	osLookup.table = (bool*)std::malloc(BoolsPerBlock * sizeof(bool));
    size_t next = 0;
    ramQueue<tuple> queue(ItemsPerBlock);
    tuple v;
    v.n = *firstNode;//get first Node
    v.n.dfsNum = 0;//get first Node
    std::cout<<"Starting BFS with BlockSize "<<ItemsPerBlock<<std::endl;
    



//3
 queue.enqueue(&v);



//4
    yyNode nextNode;
    


//5
while (!queue.empty()) {

        v = *queue.Bot();
        addDfsNum(v.n.dfsNum,false);
        bool has_child = true;
			i= v.i;



yyNode iterator_x;
iterator_x=v.n;
{std::cout<<"DFSNUMNERN\t"<<iterator_x.dfsNum<<"\n";}


//6
while (
(sqrt(v.n.r*v.n.r+v.n.i*v.n.i)<2)
&&i.next()//7
) { // while node got children
tuple w;
	iterator_i= i.val;
	w.n  = yyNode(&v.n, (long) -1 );



//8
if (w.n.dfsNum==-1


//9
) { //we push if its not known
                    //v_tmp.it =nullptr;
                    next++;
                    w.n.dfsNum=next;
                    


//10
std::cout<<v.n.r<<"\t"<<v.n.i<<std::endl;



                    queue.enqueue(&w);
                    addDfsNum(w.n.dfsNum,true);//its in queue,marked grey
                    //nextNode->mark = 1;
    }
    


//11
}
    


//12
        queue.dequeue();
    }
    //freeOsLookup();
}



