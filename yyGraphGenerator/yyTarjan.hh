#define MAXINT 1000
#pragma once

#include <iostream>
#include "ramStack.hh"
#include "yyNode.hh"
#include <list>
#include <vector>


size_t LongsPerBlock;
size_t BoolsPerBlock;
struct onStackLookup {
	int index_start, index_end;
	bool* table;
	onStackLookup* next = nullptr;
	~onStackLookup(){
		std::cout << "Destructing OnStackLookUp...\n";
		std::free(table);
	}};

onStackLookup osLookup;

bool isOnStack(int dfsNum) {
	onStackLookup* ptr = &osLookup;
	while (dfsNum > ptr->index_end)
	{
		ptr = ptr->next;
	}
	if (ptr->index_start == 0) {
		return ptr->table[dfsNum] == 1;
	}
	return ptr->table[dfsNum % ptr->index_start] == 1;
}

void addDfsNum(int dfsNum, bool val) {
	onStackLookup* ptr = &osLookup;
	int seen = 0;
	while (dfsNum > ptr->index_end)
	{
		if (ptr == nullptr || ptr->index_start < 0 || ptr->index_end < 0) {
			std::cerr << "pointer is null" << std::endl;
			exit(1);
		}
		if (ptr->next == nullptr) {
			ptr->next = (onStackLookup*)std::malloc(sizeof(onStackLookup));
			onStackLookup lookUp;
			ptr->next->index_start = ptr->index_end + 1;
			ptr->next->index_end = ptr->index_end + BoolsPerBlock;
			//ptr->next->table = new bool[BoolsPerBlock];
			ptr->next->table = (bool*)std::calloc(BoolsPerBlock, sizeof(bool));
			ptr->next->next = nullptr;
			if (lookUp.table == nullptr || lookUp.table == NULL) {
				std::cerr << "could not allocate memory" << std::endl;
				exit(1);
			}
		}
		ptr = ptr->next;
		seen++;
	}
	if (ptr->index_start == 0) {
		ptr->table[dfsNum] = val;
		//std::cout << dfsNum << std::endl;
		return;
	}
	//std::cout << dfsNum % ptr->index_start << std::endl;
	ptr->table[dfsNum % ptr->index_start] = val;
}

struct lowlinkLookup {
	int index_start, index_end;
	long* table;
	lowlinkLookup* next = nullptr;
~lowlinkLookup(){
		std::cout <<"Destructing LowLinkLookup...\n";
		std::free(this->table);
	}
};

lowlinkLookup llLookup;

long getLowLink(int dfsNum) {
	lowlinkLookup* ptr = &llLookup;
	while (dfsNum > ptr->index_end)
	{
		ptr = ptr->next;
	}
	if (ptr->index_start == 0) {
		return ptr->table[dfsNum];
	}
	return ptr->table[dfsNum % ptr->index_start];
}

void setLowLink(int dfsNum, long val) {
	lowlinkLookup* ptr = &llLookup;
	int i = 0;
	while (dfsNum > ptr->index_end)
	{
		if (ptr == nullptr || ptr->index_start < 0 || ptr->index_end < 0) {
			std::cerr << "pointer is null" << std::endl;
			exit(1);
		}
		if (ptr->next == nullptr) {
			ptr->next = (lowlinkLookup*)std::malloc(sizeof(onStackLookup));
			lowlinkLookup lookUp;
			ptr->next->index_start = ptr->index_end + 1;
			ptr->next->index_end = ptr->index_end + LongsPerBlock;
			//ptr->next->table = new bool[LongsPerBlock];
			ptr->next->table = (long*)std::calloc(LongsPerBlock, sizeof(long));
			ptr->next->next = nullptr;
			if (lookUp.table == nullptr || lookUp.table == NULL) {
				std::cerr << "could not allocate memory" << std::endl;
				exit(1);
			}
		}
		ptr = ptr->next;
		i++;
	}
	if (ptr->index_start == 0) {
		ptr->table[dfsNum] = val;
		//std::cout << dfsNum << std::endl;
		return;
	}
	//std::cout << dfsNum % ptr->index_start << std::endl;
	ptr->table[dfsNum % ptr->index_start] = val;
}
void freeOsLookup() {
    onStackLookup* osTmp = &osLookup;
    onStackLookup* osTmp2;
    std::free(osTmp->table);
    osTmp = osTmp->next;
    while (osTmp != nullptr) {
        osTmp2 = osTmp;
        osTmp = osTmp2->next;
        std::free(osTmp2->table);
    }
}
void freellLookup() {
    lowlinkLookup* osTmp = &llLookup;
    lowlinkLookup* osTmp2;
    std::free(osTmp->table);
    osTmp = osTmp->next;
    while (osTmp != nullptr) {
        osTmp2 = osTmp;
        osTmp = osTmp2->next;
        std::free(osTmp2->table);
    }
}
void test() {
	for (int i = 0; i < 20000; i++) {
		addDfsNum(i, true);
	}
	for (int i = 0; i < 20000; i++) {
		std::cout << isOnStack(i) << std::endl;
	}
}



struct viTuple {
	tuple t;
};

void yyTarjan(yyNode* firstNode) {



size_t BlockSize = 10000;
size_t ItemsPerBlock = 20;
ItemsPerBlock = (BlockSize/sizeof(tuple)) < ItemsPerBlock?(BlockSize/sizeof(tuple)) : ItemsPerBlock;
BoolsPerBlock = BlockSize/sizeof(bool);
LongsPerBlock = BlockSize/sizeof(long);

	llLookup.index_start = 0;
	llLookup.index_end = LongsPerBlock - 1;
	llLookup.next = nullptr;
	llLookup.table = (long*)std::malloc(LongsPerBlock * sizeof(long));
	osLookup.index_start = 0;
	osLookup.index_end = BoolsPerBlock - 1;
	osLookup.next = nullptr;
	osLookup.table = (bool*)std::malloc(BoolsPerBlock * sizeof(bool));
	int next = 0; // next index
	ramStack<tuple> stack(ItemsPerBlock);
	ramStack<viTuple> work(ItemsPerBlock); //Recursion stack
	tuple v;
	tuple w;
	int seen;
	int j;
	yyNode nextNode;
	bool recurse=true;
	viTuple vi;
	vi.t.n = *firstNode;
	//vi.seen = 0;
cIterator i;
		


			i= v.i;
	iterator_i= i.val;
	nextNode  = yyNode(&v.n, (long) next );
	work.push(&vi);
	while (!work.empty()) {
		v = work.Top()->t;
		v.n.lowLink = getLowLink(v.n.dfsNum);		//seen = work.Top()->seen; // i is next successor to process
		work.pop();



			i= v.i;
//XDDDDDDDDDDDDDDDDDDDDDDDD


		if (recurse) { //When first visiting a Vertex
			v.n.dfsNum = next;
			v.n.lowLink = next;
			setLowLink(next,next);
			stack.push(&v);
			//v.n->onStack = true;
			addDfsNum(next, true);
			next++;
		}
		recurse = false;

		yyNode nextNode = v.n;
		while (1&&i.next()&&(sqrt(v.n.r*v.n.r+v.n.i*v.n.i)<2)
) {
//Breakif + Iteration
	iterator_i= i.val;
	
nextNode = yyNode(&v.n,(long) -1);			//successor method
			//w.n = graph->head[nextPtr->val];
			tuple tmp_tpl;
			w = tmp_tpl;
			w.n = nextNode;



				w.n.lowLink=getLowLink(w.n.dfsNum);
			if (true&&w.n.dfsNum==-1




) { // search-insert here



std::cout<<v.n.r<<""<<v.n.i<<std::endl;//Usercode 1


			v.i= i;

				//Add w to recursion stack.
				viTuple v_tmp;
				v_tmp.t = v;
				//v_tmp.seen = 1;
				work.push(&v_tmp);
				viTuple v_tmp2;
				v_tmp2.t = w;
				//v_tmp2.seen = 0;
				work.push(&v_tmp2);
				recurse = true;
				break;
			}
			else if (isOnStack(w.n.dfsNum)) {
				setLowLink(v.n.dfsNum , getLowLink(v.n.dfsNum) < w.n.dfsNum ? getLowLink(v.n.dfsNum) : w.n.dfsNum);
			}
			//nextPtr = nextPtr->next;



		}



		if (recurse) continue;
		if (v.n.dfsNum == getLowLink(v.n.dfsNum)) {
			// pop loop



variable iterator_x;
iterator_x.dfsNum=v.n.dfsNum;
iterator_x.lowLink=v.n.lowLink;

{std::cout<<"==================\nLowLink:";std::cout<<iterator_x.lowLink<<"\n";}



			//std::list<adjNode*> scc;
			while (true) {
				w = *stack.Top();
				w.n.lowLink=getLowLink(w.n.dfsNum);


				//w->onStack = false;
				addDfsNum(w.n.dfsNum, false);



			yyNode iterator_j=w.n;
std::cout<<iterator_j.r<<"\t"<<iterator_j.i<<std::endl;
				//scc.push_back(w.n);

				if (w.n.dfsNum == v.n.dfsNum) {
					stack.pop(); //because v must be poped aswell, 
					break;
				}



				stack.pop(); // remove w's reference from stack 
			}
			//printScc(scc);
		}



		if (!work.empty()) {
			w = v;
			v = work.Top()->t;
			setLowLink(v.n.dfsNum, getLowLink(v.n.dfsNum) < getLowLink(w.n.dfsNum) ?  getLowLink(v.n.dfsNum) :  getLowLink(w.n.dfsNum));
			//v.n.lowLink = v.n.lowLink < w.n.lowLink ? v.n.lowLink : w.n.lowLink;
			//work.Top()->t.n.lowLink = v.n.lowLink < w.n.lowLink ? v.n.lowLink : w.n.lowLink;
		}
	}
//freeOsLookup();
//freellLookup();
}




