#define MAXINT 10000
//0
#pragma once

#include "ramStack.hh"
#include "yyNode.hh"
#include <iostream>


size_t LongsPerBlock;
size_t BoolsPerBlock;
struct onStackLookup {
	int index_start, index_end;
	bool* table;
	onStackLookup* next = nullptr;
	~onStackLookup(){
		std::cout << "Destructing OnStackLookUp...\n";
		std::free(table);
	}};

onStackLookup osLookup;

bool isOnStack(int dfsNum) {
	onStackLookup* ptr = &osLookup;
	while (dfsNum > ptr->index_end)
	{
		ptr = ptr->next;
	}
	if (ptr->index_start == 0) {
		return ptr->table[dfsNum] == 1;
	}
	return ptr->table[dfsNum % ptr->index_start] == 1;
}

void addDfsNum(int dfsNum, bool val) {
	onStackLookup* ptr = &osLookup;
	int seen = 0;
	while (dfsNum > ptr->index_end)
	{
		if (ptr == nullptr || ptr->index_start < 0 || ptr->index_end < 0) {
			std::cerr << "pointer is null" << std::endl;
			exit(1);
		}
		if (ptr->next == nullptr) {
			ptr->next = (onStackLookup*)std::malloc(sizeof(onStackLookup));
			onStackLookup lookUp;
			ptr->next->index_start = ptr->index_end + 1;
			ptr->next->index_end = ptr->index_end + BoolsPerBlock;
			//ptr->next->table = new bool[BoolsPerBlock];
			ptr->next->table = (bool*)std::calloc(BoolsPerBlock, sizeof(bool));
			ptr->next->next = nullptr;
			if (lookUp.table == nullptr || lookUp.table == NULL) {
				std::cerr << "could not allocate memory" << std::endl;
				exit(1);
			}
		}
		ptr = ptr->next;
		seen++;
	}
	if (ptr->index_start == 0) {
		ptr->table[dfsNum] = val;
		//std::cout << dfsNum << std::endl;
		return;
	}
	//std::cout << dfsNum % ptr->index_start << std::endl;
	ptr->table[dfsNum % ptr->index_start] = val;
}

void freeOsLookup() {
    onStackLookup* osTmp = &osLookup;
    onStackLookup* osTmp2;
    std::free(osTmp->table);
    osTmp = osTmp->next;
    while (osTmp != nullptr) {
        osTmp2 = osTmp;
        osTmp = osTmp2->next;
        std::free(osTmp2->table);
    }
}



//1
void yyDFS(yyNode* firstNode) {
cIterator i;
		size_t BlockSize = 10000;
size_t ItemsPerBlock = 20;
ItemsPerBlock = (BlockSize/sizeof(tuple)) < ItemsPerBlock?(BlockSize/sizeof(tuple)) : ItemsPerBlock;
BoolsPerBlock = BlockSize/sizeof(bool);
LongsPerBlock = BlockSize/sizeof(long);




//2
    std::cout<<"Starting DFS with BlockSize "<<ItemsPerBlock<<std::endl;
    osLookup.index_start = 0;
	osLookup.index_end = BoolsPerBlock - 1;
	osLookup.next = nullptr;
	osLookup.table = (bool*)std::malloc(BoolsPerBlock * sizeof(bool));
    // Initialize an empty dfs stack and push the "start" node into it
    ramStack<tuple> stack(ItemsPerBlock);
    tuple v;
    v.n = *firstNode;
    v.n.dfsNum=0;
    size_t next= 0;
    


//3
stack.push(&v);
    yyNode elem, nextNode;



//4
while (!stack.empty()) {
        v= *stack.Top();
        stack.pop();



			i= v.i;



yyNode iterator_x;
iterator_x=v.n;
{std::cout<<"DFSNUMNERN\t"<<iterator_x.dfsNum<<"\n";}


//5
if (isOnStack(v.n.dfsNum)



//6
) {
            continue;
        }
        //elem->mark = 1;



//7
        while (



(sqrt(v.n.r*v.n.r+v.n.i*v.n.i)<2)
&&i.next()//8

) {



	iterator_i= i.val;
	//9
tuple w;
        w.n  = yyNode(&v.n, (long) -1 );
        if (w.n.dfsNum==-1



//10
){                //v_tmp.it = nullptr;



//11
std::cout<<v.n.r<<"\t"<<v.n.i<<std::endl;
			v.i= i;

        stack.push(&v);
        next++;
        w.n.dfsNum=next;
        stack.push(&w);
        addDfsNum(v.n.dfsNum, true);
        }
        }
        


//12
addDfsNum(v.n.dfsNum, false);
        }
        //freeOsLookup();
}


